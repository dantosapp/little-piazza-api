<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.3.3
    </div>
    <strong>Copyright &copy; <?= date("Y") ?> <a href="http://littlepiazza.com.au">Little Piazza</a>.</strong> All rights
    reserved.
</footer>