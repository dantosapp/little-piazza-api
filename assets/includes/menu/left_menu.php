<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <img src="assets/img/dummy_profile.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
            <p><?= $_SESSION["lt_main_name"] ?></p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li><a href="Categories"><i class="fa fa-circle-o"></i> <span>Categories</span></a></li>
        <li><a href="Products"><i class="fa fa-circle-o"></i> <span>Products</span></a></li>
        <li><a href="Schedule"><i class="fa fa-circle-o"></i> <span>Restaurant Schedule</span></a></li>
        <li><a href="Usersdiscount"><i class="fa fa-circle-o"></i> <span>Check Users Discount</span></a></li>
        <li><a href="Settings"><i class="fa fa-circle-o"></i> <span>Settings</span></a></li>
        <li><a href="Policy"><i class="fa fa-circle-o"></i> <span>Policy</span></a></li>
        <li class="active treeview">
            <a href="#">
                <i class="fa fa-object-group"></i> <span>Websit</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li class="active"><a href="Cliente"><i class="fa fa-circle-o"></i> Nuevo</a></li>
                <li><a href="Clientes"><i class="fa fa-circle-o"></i> Listado</a></li>
            </ul>
        </li>

    </ul>
</section>
<!-- /.sidebar -->