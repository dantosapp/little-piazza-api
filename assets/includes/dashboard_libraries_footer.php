<!-- jQuery 2.2.0 -->
<script src="assets/includes/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="assets/includes/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="assets/includes/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="assets/includes/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="assets/includes/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="assets/includes/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="assets/includes/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="assets/includes/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="assets/includes/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="assets/includes/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="assets/includes/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="assets/includes/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="assets/includes/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="assets/includes/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="assets/includes/dist/js/demo.js"></script>
<!-- Select 2 -->
<script type="text/javascript" src="assets/includes/plugins/select2/select2.full.min.js"></script>
<!-- Sweet Alert --> 
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>