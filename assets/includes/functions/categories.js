
    $("form#category_form").submit(function(e) {
        e.preventDefault()

        let name     = $("#cat_name").val();

        if (name.length == 0) {
            swal({
                title: "The field NAME is mandatory",
                icon: "error"
            })
            return;
        }

        var formData = new FormData(this);

        $.ajax({
                url: "Categories/updateCategory",
                type: 'POST',
                enctype: 'multipart/form-data',
                data: formData,
                success: function (data) {

                    var r = jQuery.parseJSON(data);

                    if (r.success == true) {
                        swal({
                            title: r.message,
                            icon: "success"
                        }).then((valid) => {
                            location.reload();
                    })
                } else {
                    swal({
                        title: r.message,
                        icon: "error"
                    })
                }
            },
            error: function (error) {
                console.log(error);
                swal({
                    title: "Something's wrong in your request, please try again",
                    icon: "error"
                })
            },
            cache: false,
            contentType: false,
            processData: false
        })
    });