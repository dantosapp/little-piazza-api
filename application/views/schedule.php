<!DOCTYPE html>
<html>
<head>
    <?php require_once "assets/includes/dashboard_head.php" ?>
    <!-- data tables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        <?php require_once "assets/includes/menu/top_menu.php" ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <?php require_once "assets/includes/menu/left_menu.php" ?>
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Restaurant Schedule
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Restaurant Schedule</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- Main Form -->
                <div class="col-sm-12">
                    <div class="box-body">
                        <p class="text-center text-danger"><b><?php echo validation_errors(); ?></b></p>
                        <p class="text-center <?= (isset($text_response) ? $text_response : "") ?>"><b><?= $process_message ?></b></p>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Restaurant Schedule</h3>
                    </div>
                    <div class="box-body">

                        <?php echo form_open_multipart('Schedule'); ?>
                        <!-- SUNDAY -->
                        <input type="hidden" name="id" value="<?= $record->id ?>">
                        <div class="col-sm-12">
                            <div class="col-sm-12 lead">
                                <b>Sunday</b>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Open for Lunch</label>
                                    <input type="time" class="form-control" name="sunday_open_lunch" value="<?= $record->sunday_open_lunch ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Close for Lunch</label>
                                    <input type="time" class="form-control" name="sunday_close_lunch" value="<?= $record->sunday_close_lunch ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Open Dinner</label>
                                    <input type="time" class="form-control" name="sunday_open" value="<?= $record->sunday_open ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Close Dinner</label>
                                    <input type="time" class="form-control" name="sunday_close" value="<?= $record->sunday_close ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                        </div>

                        <!-- MONDAY -->
                        <div class="col-sm-12">
                            <div class="col-sm-12 lead">
                                <b>Monday</b>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Open for Lunch</label>
                                    <input type="time" class="form-control" name="monday_open_lunch" value="<?= $record->monday_open_lunch ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Close for Lunch</label>
                                    <input type="time" class="form-control" name="monday_close_lunch" value="<?= $record->monday_close_lunch ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Open for Dinner</label>
                                    <input type="time" class="form-control" name="monday_open" value="<?= $record->monday_open ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Close for Dinner</label>
                                    <input type="time" class="form-control" name="monday_close" value="<?= $record->monday_close ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                        </div>

                        <!-- TUESDAY -->
                        <div class="col-sm-12">
                            <div class="col-sm-12 lead">
                                <b>Tuesday</b>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Open for Lunch</label>
                                    <input type="time" class="form-control" name="tuesday_open_lunch" value="<?= $record->tuesday_open_lunch ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Close for Lunch</label>
                                    <input type="time" class="form-control" name="tuesday_close_lunch" value="<?= $record->tuesday_close_lunch ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Open for Dinner</label>
                                    <input type="time" class="form-control" name="tuesday_open" value="<?= $record->tuesday_open ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Close for Dinner</label>
                                    <input type="time" class="form-control" name="tuesday_close" value="<?= $record->tuesday_close ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                        </div>

                        <!-- WEDNESDAY -->
                        <div class="col-sm-12">
                            <div class="col-sm-12 lead">
                                <b>Wednesday</b>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Open for Lunch</label>
                                    <input type="time" class="form-control" name="wednesday_open_lunch" value="<?= $record->wednesday_open_lunch ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Close for Lunch</label>
                                    <input type="time" class="form-control" name="wednesday_close_lunch" value="<?= $record->wednesday_close_lunch ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Open for Dinner</label>
                                    <input type="time" class="form-control" name="wednesday_open" value="<?= $record->wednesday_open ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Close for Dinner</label>
                                    <input type="time" class="form-control" name="wednesday_close" value="<?= $record->wednesday_close ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                        </div>

                        <!-- THURSDAY -->
                        <div class="col-sm-12">
                            <div class="col-sm-12 lead">
                                <b>Thursday</b>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Open for Lunch</label>
                                    <input type="time" class="form-control" name="thursday_open_lunch" value="<?= $record->thursday_open_lunch ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Close for Lunch</label>
                                    <input type="time" class="form-control" name="thursday_close_lunch" value="<?= $record->thursday_close_lunch ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Open for Dinner</label>
                                    <input type="time" class="form-control" name="thursday_open" value="<?= $record->thursday_open ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Close for Dinner</label>
                                    <input type="time" class="form-control" name="thursday_close" value="<?= $record->thursday_close ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                        </div>

                        <!-- FRIDAY -->
                        <div class="col-sm-12">
                            <div class="col-sm-12 lead">
                                <b>Friday</b>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Open for Lunch</label>
                                    <input type="time" class="form-control" name="friday_open_lunch" value="<?= $record->friday_open_lunch ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Close for Lunch</label>
                                    <input type="time" class="form-control" name="friday_close_lunch" value="<?= $record->friday_close_lunch ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Open for Dinner</label>
                                    <input type="time" class="form-control" name="friday_open" value="<?= $record->friday_open ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Close for Dinner</label>
                                    <input type="time" class="form-control" name="friday_close" value="<?= $record->friday_close ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                        </div>

                        <!-- SATURDAY -->
                        <div class="col-sm-12">
                            <div class="col-sm-12 lead">
                                <b>Saturday</b>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Open for Lunch</label>
                                    <input type="time" class="form-control" name="saturday_open_lunch" value="<?= $record->saturday_open_lunch ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Close for Lunch</label>
                                    <input type="time" class="form-control" name="saturday_close_lunch" value="<?= $record->saturday_close_lunch ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Open for Dinner</label>
                                    <input type="time" class="form-control" name="saturday_open" value="<?= $record->saturday_open ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group has-feedback">
                                    <label>Close for Dinner</label>
                                    <input type="time" class="form-control" name="saturday_close" value="<?= $record->saturday_close ?>">
                                    <i class="fa fa-tachometer form-control-feedback"></i>
                                </div>
                            </div>
                        </div>





                        <div class="col-sm-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
                            </div>
                        </div>
                        </form>

                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <?php include("assets/includes/footer.php") ?>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane" id="control-sidebar-home-tab">
            </div>
            <!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->
            <!-- Settings tab content -->
            <div class="tab-pane" id="control-sidebar-settings-tab">
            </div>
            <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->

    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php require_once "assets/includes/dashboard_libraries_footer.php" ?>

<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

<script>
    $('#table').DataTable();
</script>
<!-- PICTURE MANAGER -->
<script>
    //DROP THE PICTURE JUST SELECTED

    //DROP THE LOGO
    $(".delete_picture").click(function() {
        var idImagen = this.id;
        console.log(this.id);
        var imageName = idImagen.split("_");
        var contenedorBorrar = '#'+imageName[1];

        var contenedor = '#picture_view_'+imageName[1]+'';
        $(contenedor).attr('src', 'assets/img/iconos/ms-icon-310x310.png');
        $(contenedorBorrar).val('');
    });


    function readURL(input, id) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var contenedor = "";
            switch (id) {
                case "development_l":
                    contenedor = "#picture_view_development";
                    break;
                case "builder_l":
                    contenedor = "#picture_view_builder";
                    break;
                default:
                    contenedor = '#picture_view_'+id+'';
                    break;
            }

            reader.onload = function (e) { $(contenedor).attr('src', e.target.result); };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".picture_container").on('change', ".load_picture", function() {
        var id = this.id;
        readURL(this, id);
    });
</script>
</body>
</html>
