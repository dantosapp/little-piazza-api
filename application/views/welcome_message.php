<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<?php require_once "assets/includes/head.php" ?>
</head>
<body class="hold-transition login-page"  style="background: transparent url('assets/img/background_welcome.jpg') repeat fixed 0% 0% / 100% 100%; transition: background 2.5s linear 0s;">

<?php echo form_open('welcome'); ?>
<div class="login-box">
	<div class="login-logo">
		<a href="#"><b>Little Piazza</b></a>
	</div>
	<!-- /.login-logo -->
	<div class="login-box-body">
		<p class="text-center text-danger"><b><?php echo (isset($error_login) ? $error_login : "") ?></b></p>
		<p class="login-box-msg">Sign in to start your session</p>

		<?php echo form_open('Welcome'); ?>
			<div class="form-group has-feedback">
				<input type="text" name="usr_name" value="<?php echo set_value('usr_name'); ?>" class="form-control" placeholder="Username">
				<span class="glyphicon glyphicon-user form-control-feedback"></span>
			</div>
			<div class="form-group has-feedback">
				<input type="password" name="usr_password" <?php echo set_value('password'); ?> class="form-control" placeholder="Password">
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
			</div>
			<div class="row">
				<div class="col-xs-8">

				</div>
				<!-- /.col -->
				<div class="col-xs-4">
					<button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
				</div>
				<!-- /.col -->
			</div>
		</form>
		<b class="text-danger"><?php echo validation_errors(); ?></b>

	</div>
	<!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.0 -->
<script src="assets/includes/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="assets/includes/bootstrap/js/bootstrap.min.js"></script>



</body>
</html>