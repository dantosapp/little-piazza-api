<!DOCTYPE html>
<html>
<head>
    <?php require_once "assets/includes/dashboard_head.php" ?>
    <!-- data tables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        <?php require_once "assets/includes/menu/top_menu.php" ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <?php require_once "assets/includes/menu/left_menu.php" ?>
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- Main Form -->
                <div class="col-sm-12">
                    <div class="box-body">
                        <p class="text-center text-danger"><b><?php echo validation_errors(); ?></b></p>
                        <p class="text-center <?= (isset($text_response) ? $text_response : "") ?>"><b><?= $process_message ?></b></p>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Customers Discount</h3>
                    </div>
                    <div class="box-body">
                        <!-- List -->
                        <div class="col-sm-12">
                            <table id="table" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th class="text-center">Id</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">App</th>
                                    <th class="text-center">Email</th>
                                    <th class="text-center">Mobile</th>
                                    <th class="text-center">Order #</th>
                                    <th class="text-center">Order ID</th>
                                    <th class="text-center">Delivery Type</th>
                                    <th class="text-center">Total</th>
                                    <th class="text-center">Discount</th>
                                    <th class="text-center">Order Date</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th class="text-center">Id</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">App</th>
                                    <th class="text-center">Email</th>
                                    <th class="text-center">Mobile</th>
                                    <th class="text-center">Order #</th>
                                    <th class="text-center">Order ID</th>
                                    <th class="text-center">Delivery Type</th>
                                    <th class="text-center">Total</th>
                                    <th class="text-center">Discount</th>
                                    <th class="text-center">Order Date</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?php foreach ($list as $key => $value) { ?>
                                    <tr>
                                        <td><?= $value->customer_id ?></td>
                                        <td><?= $value->cust_name.' '.$value->cust_surname ?></td>
                                        <td><?= ($value->os != "" ? strtoupper($value->os) : "UNDEFINED") ?></td>
                                        <td><?= $value->cust_emailaddress ?></td>
                                        <td><?= $value->cust_mobphone ?></td>
                                        <td><?= (isset($value->ord_number) ? $value->ord_number : "") ?></td>
                                        <td><?= $value->orderId ?></td>
                                        <?php
                                            $deliveryType = "";
                                            switch ($value->ord_deliverytype) {
                                                case 0:
                                                    $deliveryType = "Delivery";
                                                    break;
                                                default:
                                                    $deliveryType = "PickUp";
                                            }
                                        ?>
                                        <td><?= $deliveryType ?></td>
                                        <td><?= $value->ord_grosstotal ?></td>
                                        <td><?= $value->ord_discounttotal ?></td>
                                        <td><?= date("d/m/Y - H:i", strtotime($value->created_at)) ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <?php include("assets/includes/footer.php") ?>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane" id="control-sidebar-home-tab">
            </div>
            <!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->
            <!-- Settings tab content -->
            <div class="tab-pane" id="control-sidebar-settings-tab">
            </div>
            <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->

    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php require_once "assets/includes/dashboard_libraries_footer.php" ?>

<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

<script>
    $('#table').DataTable();
</script>
<!-- PICTURE MANAGER -->
<script>
    //DROP THE PICTURE JUST SELECTED

    //DROP THE LOGO
    $(".delete_picture").click(function() {
        var idImagen = this.id;
        console.log(this.id);
        var imageName = idImagen.split("_");
        var contenedorBorrar = '#'+imageName[1];

        var contenedor = '#picture_view_'+imageName[1]+'';
        $(contenedor).attr('src', 'assets/img/iconos/ms-icon-310x310.png');
        $(contenedorBorrar).val('');
    });


    function readURL(input, id) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var contenedor = "";
            switch (id) {
                case "development_l":
                    contenedor = "#picture_view_development";
                    break;
                case "builder_l":
                    contenedor = "#picture_view_builder";
                    break;
                default:
                    contenedor = '#picture_view_'+id+'';
                    break;
            }

            reader.onload = function (e) { $(contenedor).attr('src', e.target.result); };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".picture_container").on('change', ".load_picture", function() {
        var id = this.id;
        readURL(this, id);
    });
</script>
</body>
</html>
