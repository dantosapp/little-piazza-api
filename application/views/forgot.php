<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <?php require_once "assets/includes/head.php" ?>
</head>
<body class="hold-transition login-page"  style="background: transparent url('assets/img/background_welcome.jpg') repeat fixed 0% 0% / 100% 100%; transition: background 2.5s linear 0s;">


<div class="login-box">
    <div class="login-logo">
        <a href="#"><b><?= $page_title ?></b></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <?php if ($success == 0) { ?>
            <p class="text-danger"><?= $message ?></p>
        <?php } else { ?>
            <p class="text-center text-danger"><b><?php echo (isset($error_login) ? $error_login : "") ?></b></p>
            <p class="login-box-msg">Change The Password</p>

            <?php echo form_open('Forgot_password'); ?>
            <div class="form-group has-feedback">
                <input type="hidden" name="customers_id" value="<?= $customer ?>">
                <input type="hidden" name="token" value="<?= $token ?>">
                <input type="password" name="password1" value="<?php echo set_value('password1'); ?>" class="form-control" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password2" <?php echo set_value('password2'); ?> class="form-control" placeholder="Confirme Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">

                <!-- /.col -->
                <div class="col-xs-4 col-sm-push-8">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Send</button>
                </div>
                <!-- /.col -->
            </div>
            </form>

        <?php } ?>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.0 -->
<script src="assets/includes/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="assets/includes/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="assets/includes/plugins/iCheck/icheck.min.js"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
</body>
</html>