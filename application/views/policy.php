<!DOCTYPE html>
<html>
<head>
    <?php require_once "assets/includes/dashboard_head.php" ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        <?php require_once "assets/includes/menu/top_menu.php" ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <?php require_once "assets/includes/menu/left_menu.php" ?>
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Policy
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Policy</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- Main Form -->
                <div class="col-sm-12">
                    <div class="box-body">
                        <p class="text-center text-danger"><b><?php echo validation_errors(); ?></b></p>
                        <p class="text-center <?= (isset($text_response) ? $text_response : "") ?>"><b><?= $process_message ?></b></p>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Policy</h3>
                    </div>
                    <div class="box-body">

                        <?php echo form_open_multipart('Policy'); ?>
                        <!-- Form create/edit Categoria -->
                        <input type="hidden" name="id" value="<?= $record->id ?>">
                        <div class="col-sm-12">
                            <div class="form-group has-feedback">
                                <label>Policy</label> <span class="text-danger"><?php echo form_error('name'); ?></span>
                                <textarea name="policy" class="form-control textarea" rows="50"><?= $record->policy ?></textarea>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
                            </div>
                        </div>
                        </form>

                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <?php include("assets/includes/footer.php") ?>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane" id="control-sidebar-home-tab">
            </div>
            <!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->
            <!-- Settings tab content -->
            <div class="tab-pane" id="control-sidebar-settings-tab">
            </div>
            <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->

    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php require_once "assets/includes/dashboard_libraries_footer.php" ?>


</body>
</html>
