<!DOCTYPE html>
<html>
<head>
    <?php require_once "assets/includes/dashboard_head.php" ?>
    <!-- data tables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        <?php require_once "assets/includes/menu/top_menu.php" ?>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <?php require_once "assets/includes/menu/left_menu.php" ?>
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- Main Form -->
                <div class="col-sm-12">
                    <div class="box-body">
                        <p class="text-center text-danger"><b><?php echo validation_errors(); ?></b></p>
                        <p class="text-center <?= (isset($text_response) ? $text_response : "") ?>"><b><?= $process_message ?></b></p>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Products List</h3>
                    </div>
                    <div class="box-body">

                        <?php echo form_open_multipart('Products'); ?>
                        <!-- Form create/edit Categoria -->
                        <input type="hidden" name="prd_id" value="<?= (isset($_GET["id"]) ? $_GET["id"] : 0) ?>">
                        <div class="col-sm-4">
                            <div class="form-group has-feedback">
                                <label>Name</label> <span class="text-danger"><?php echo form_error('name'); ?></span>
                                <input type="text" class="form-control" value="<?php echo set_value("name", (isset($record->prd_name) ? $record->prd_name : "")) ?>" disabled maxlength="250">
                                <span class="glyphicon glyphicon-pencil form-control-feedback"></span>
                            </div>
                            <div class="form-group has-feedback picture_container">
                                <label>Picture</label>
                                <input type="file" id="logo" name="photo" class="form-control load_picture" accept="file_extension|image/*|" capture>
                                <img id="picture_view_logo" src="<?php echo isset($record->picture) ? "assets/img/products/".$record->picture : 'assets/img/placeholder.png' ?>" class="img-responsive thumbnail" ng-file-select="onFileSelect($files)">
                                <i class="fa fa-user form-control-feedback"></i>

                            </div>
                            <div class="form-group">
                                <button type="submit" <?= $button ?> class="btn btn-primary btn-block btn-flat">Save</button>
                            </div>
                        </div>
                        </form>

                        <!-- List -->
                        <div class="col-sm-8">
                            <table id="table" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Group</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Group</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?php foreach ($list as $key => $value) { ?>
                                    <tr>
                                        <td><?= $value->prd_id ?></td>
                                        <td><?= $value->prd_name ?></td>
                                        <td><?= $value->category ?></td>
                                        <td><?= $value->group_name ?></td>
                                        <td><a href="Products?id=<?= $value->prd_id?>" class="btn btn-info btn-sm btn-block">Edit</a></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <?php include("assets/includes/footer.php") ?>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane" id="control-sidebar-home-tab">
            </div>
            <!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->
            <!-- Settings tab content -->
            <div class="tab-pane" id="control-sidebar-settings-tab">
            </div>
            <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->

    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php require_once "assets/includes/dashboard_libraries_footer.php" ?>

<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

<script>
    $('#table').DataTable();
</script>
<!-- PICTURE MANAGER -->
<script>
    //DROP THE PICTURE JUST SELECTED

    //DROP THE LOGO
    $(".delete_picture").click(function() {
        var idImagen = this.id;
        console.log(this.id);
        var imageName = idImagen.split("_");
        var contenedorBorrar = '#'+imageName[1];

        var contenedor = '#picture_view_'+imageName[1]+'';
        $(contenedor).attr('src', 'assets/img/iconos/ms-icon-310x310.png');
        $(contenedorBorrar).val('');
    });


    function readURL(input, id) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var contenedor = "";
            switch (id) {
                case "development_l":
                    contenedor = "#picture_view_development";
                    break;
                case "builder_l":
                    contenedor = "#picture_view_builder";
                    break;
                default:
                    contenedor = '#picture_view_'+id+'';
                    break;
            }

            reader.onload = function (e) { $(contenedor).attr('src', e.target.result); };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".picture_container").on('change', ".load_picture", function() {
        var id = this.id;
        readURL(this, id);
    });
</script>
</body>
</html>
