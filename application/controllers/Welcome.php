<?php
session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$pageTitle = "Little Piazza";
		$data["page_title"] = $pageTitle;
		$this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');

		$this->form_validation->set_rules('usr_name', 'Username', 'trim|required');
		$this->form_validation->set_rules('usr_password', 'Password', 'required|min_length[6]');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('welcome_message', $data);
		} else {
			$this->load->model('User_model');
			$userLogin = $this->User_model->login($_POST);
			if (!empty($userLogin)) {
				$this->User_model->createSession($userLogin);
				$data["page_title"] = $pageTitle;
				$this->load->view('dashboard', $data);
			} else {
				$data["error_login"] = "User and password combination is wrong";
				$this->load->view('welcome_message', $data);
			}
		}
	}
}
