<?php
session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Schedule extends CI_Controller {

    public function index()
    {
        if (!isset($_SESSION['lt_main_name'])) {
            redirect(base_url(), 'refresh');
            die();
        }

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        $this->form_validation->set_rules('monday_open', 'Monday Open', 'trim|required');

        $pageTitle = "Little Piazza";
        $data["page_title"] = $pageTitle;

        $data["process_message"] = "";
        $data["form_completed"] = "";

        $this->load->model("Schedule_model");
        $schedule = $this->Schedule_model->getRecordById(1);
        $data["record"] = $schedule;

        if ($this->form_validation->run() == FALSE)  {
            $this->load->view('schedule', $data);
        } else {
            unset($_POST["_wysihtml5_mode"]);
            $this->load->model("Schedule_model");
            $this->Schedule_model->updateRecord($_POST);
            $data["page_title"] = $pageTitle;
            $record = $this->Schedule_model->getRecordById(1);
            $data["record"] = $record;
//            redirect("Settings", "refresh");
            $this->load->view('schedule', $data);
        }

    }
}
