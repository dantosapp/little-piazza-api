<?php
session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {

    public function index()
    {
        if (!isset($_SESSION['lt_main_name'])) {
            redirect(base_url(), 'refresh');
            die();
        }

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        $this->form_validation->set_rules('first_time_percentage', 'Percentage', 'trim|required|numeric');
        $this->form_validation->set_rules('first_time_amount', 'Amount', 'trim|required|numeric');
        $this->form_validation->set_rules('minimal_amount', 'Amount', 'trim|required|numeric');
        $this->form_validation->set_rules('initial_message', 'Initial Message', 'trim|required');

        $pageTitle = "Little Piazza";
        $data["page_title"] = $pageTitle;

        $data["process_message"] = "";
        $data["form_completed"] = "";

        $this->load->model("Appsettings_model");
        $policy = $this->Appsettings_model->getAll();
        $data["record"] = $policy;

        if ($this->form_validation->run() == FALSE)  {
            $this->load->view('settings', $data);
        } else {
            unset($_POST["_wysihtml5_mode"]);
            $this->load->model("Appsettings_model");
            $this->Appsettings_model->update($_POST);
            $data["page_title"] = $pageTitle;
            $policy = $this->Appsettings_model->getAll();
            $data["record"] = $policy;
//            redirect("Settings", "refresh");
            $this->load->view('settings', $data);
        }
        
    }
}
