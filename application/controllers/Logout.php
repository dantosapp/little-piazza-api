<?php
session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

    public function index()
    {

        $_SESSION = array();

        session_destroy();

        $this->load->view('logout');
        redirect(base_url(), 'refresh');
    }
}
