<?php
session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (!isset($_SESSION['lt_main_name'])) {
            redirect(base_url(), 'refresh');
            die();
        }
    }

    public function index()
    {
        $pageTitle = "Little Piazza";
        $data["page_title"] = $pageTitle;
        
        $this->load->model("Category_model");
        $categories = $this->Category_model->getAll();
        $data["list"] = $categories;

        // Product Groups
        $this->load->model("ProductGroup_model");
        $groups = $this->ProductGroup_model->getAll();
        $data["groups"] = $groups;

        $data["process_message"] = "";
        $data["form_completed"] = "";

        $data["button"] = "disabled";
        if (isset($_GET["id"]) && $_GET["id"] > 0) {
            $data["button"] = "";
            $category = $this->Category_model->getRecordById($_GET["id"]);
            $data["record"] = $category;
        }

        $this->load->view('categories', $data);
    }

    public function updateCategory() {
        if (isset($_POST["cat_id"]) && isset($_POST["cat_name"])) {

            $this->load->model("Category_model");
            $this->load->model("Date_model");
            $today = $this->Date_model->getLocalTime();
            $_POST["updated_at"] = $today;

            $this->db->trans_begin();
            $record = $this->Category_model->create($_POST, $_FILES);

            if ($this->db->trans_status() === FALSE) { 
                $success = false;
                $message = "La información no pudo ser guardada, por favor intente de nuevo";
                $this->db->trans_rollback();
                 
            } else {
                $success = true;
                $this->db->trans_commit();
                $message = "La información se ha guardado exitosamente";
            }
            
            $response = array(
                "success" => $success,
                "message" => $message
            );
        } else {
            $response = array(
                "success" => false,
                "message" => "Wrong parameters received, please try again"
            );
        }
        $this->sendResponse($response);
    }

    private function sendResponse($data) {
        $response = json_encode($data, true);
        print $response;
    }
}
