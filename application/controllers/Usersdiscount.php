<?php
session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Usersdiscount extends CI_Controller {

    public function index()
    {
        if (!isset($_SESSION['lt_main_name'])) {
            redirect(base_url(), 'refresh');
            die();
        }

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        $pageTitle = "Little Piazza";
        $data["page_title"] = $pageTitle;
        $this->load->model("Orderdevice_model");
        $products = $this->Orderdevice_model->getAll();
        $data["list"] = $products;

        $data["process_message"] = "";
        $data["form_completed"] = "";

        $this->form_validation->set_rules('prd_id', 'Product Picture', 'trim|required');

        $this->load->model('Product_model');

        $data["button"] = "disabled";
        if (isset($_GET["id"]) && $_GET["id"] > 0) {
            $data["button"] = "";
            $product = $this->Product_model->getRecord($_GET["id"]);
            $data["record"] = $product;
        }

        if ($this->form_validation->run() == FALSE)  {
            $this->load->view('users_discount', $data);
        } else {
            $this->load->model("Products_picture_model");
            $saveRecord = $this->Products_picture_model->create($_POST, $_FILES);

            if ($saveRecord["success"]) {
                $data["page_title"] = $pageTitle;
                //$this->load->view('Clientes', $data);
                redirect('users_discount', $data);
            } else {
                $data["process_message"] = $saveRecord["message"];
                $this->load->view('users_discount', $data);
            }
        }


    }
}
