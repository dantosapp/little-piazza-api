<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model("Logs_model");
        $expire = date('l jS \of F Y h:i:s e', strtotime("+ 30 days"));
        header('Content-Type: application/json');
        header("Expires: ".$expire."");
    }

    //******* USER METHODS
    function forgotPassword() {
        if (isset($_POST['key']) && $_POST['key'] == '05130d60b3bee9339c7b4fb9e6fc83b6' && isset($_POST["cust_emailaddress"]) ) {

            $this->load->model("Customers_model");
            $customer = $this->Customers_model->getRecordByEmail($_POST["cust_emailaddress"]);
            if (!empty($customer)) {
                $this->load->model("Forgot_model");
                $emailBody = $this->Forgot_model->createRequest($customer);

                require 'assets/includes/email/PHPMailerAutoload.php';
                $this->sendEmail($customer->cust_emailaddress, "Password Request", $emailBody);
                $response = array("success" => true, "message" => "We just sent you an email with the instructions.");
            } else {
                $response = array("success" => false, "message" => "The email doesn't exists in our records");
            }

        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }
        $response = json_encode($response);
        print $response;
    }

    function login() {
        if (isset($_POST['key']) && $_POST['key'] == '05130d60b3bee9339c7b4fb9e6fc83b6' && isset($_POST["cust_emailaddress"]) && isset($_POST["cust_password"])) {

            unset($_POST["key"]);
            $this->load->model("Customers_model");
            if (isset($_POST["social"])) {
                $this->load->model("Customeraddress_model");
                //check if user exists
                $checkExists = $this->Customers_model->getRecordByEmail($_POST["cust_emailaddress"]);

                if (!empty($checkExists)) {
                    $address = $this->Customeraddress_model->getAddressByCustomer($checkExists->cust_id);
                    $completed = true;
                    if (empty($address)) {
                        $completed = false;
                    }
                    $response = array("success" => true, "completed" => $completed, "content" => $checkExists, "message" => "Login successful");
                } else {
                    unset($_POST["social"]);
                    $_POST["cust_restid"] = 6;
                    $_POST["cust_ismember"] = 1;
                    $_POST["cust_createdip"] = "iOS App";
                    $_POST["cust_createdate"] = date("Y-m-d H:i:s");
                    $record = $this->Customers_model->registerFacebookUser($_POST);
                    //$content = array("cust_name" => $_POST["cust_name"], "cust_emailaddress" => $_POST["cust_emailaddress"], "cust_surname" => $_POST["cust_surname"], "cust_id" => $record);
                    //$address = $this->Customeraddress_model->getAddressByCustomer($record);
                    $result = $this->Customers_model->getRecordByEmail($_POST["cust_emailaddress"]);
                    $completed = true;
                    if ($result->adr_id == null) {
                        $result->sbrb_deliveryfreelimit = 0;
                        $completed = false;
                    }

                    if (!($result->sbrb_deliveryfreelimit > 0)) {
                        $result->sbrb_deliveryfreelimit = 0;
                    }
                    $response = array("success" => true, "completed" => $completed, "content" => $result, "message" => "Login successful" );
                }
            } else {
                $result = $this->Customers_model->user_login($_POST);
                if (!empty($result)) {
                    $completed = true;
                    if ($result->adr_id == null) {
                        $result->sbrb_deliveryfreelimit = 0;
                        $completed = false;
                    }
                    $response =  array("success" => true, "completed" => $completed, "content" => $result, "message" => "Login successful");
                } else {
                    $response =  array("success" => false, "message" => "User and password combination is incorrect");
                }
            }
        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }
        $response = json_encode($response);
        print $response;
    }

    function loginNew() {
        if (isset($_POST["key"]) && $_POST["key"] == $this->getKey() && isset($_POST["cust_emailaddress"])
            && isset($_POST["cust_password"])) {

            unset($_POST["key"]);
            $this->load->model("Customers_model");
            $message = "";
            //Check First if it is a customer
            if (isset($_POST["social"])) {
                $this->load->model("Customeraddress_model");
                $checkExists = $this->Customers_model->getRecordByEmail($_POST["cust_emailaddress"]);

                if (!empty($checkExists)) {
                    $address = $this->Customeraddress_model->getAddressByCustomer($checkExists->cust_id);
                    $completed = true;
                    if (empty($address)) {
                        $completed = false;
                    }

                    $profiles[] = array("completed" => $completed, "content" => $checkExists, "profile" => "Customer");

                } else {
                    unset($_POST["social"]);
                    $_POST["cust_restid"] = 6;
                    $_POST["cust_ismember"] = 1;
                    $_POST["cust_createdip"] = "iOS App";
                    $_POST["cust_createdate"] = date("Y-m-d H:i:s");
                    $record = $this->Customers_model->registerFacebookUser($_POST);

                    $result = $this->Customers_model->getRecordByEmail($_POST["cust_emailaddress"]);
                    $completed = true;
                    if ($result->adr_id == null) {
                        $result->sbrb_deliveryfreelimit = 0;
                        $completed = false;
                    }

                    if (!($result->sbrb_deliveryfreelimit > 0)) {
                        $result->sbrb_deliveryfreelimit = 0;
                    }
                    $profiles[] = array("completed" => $completed, "content" => $result, "message" => "Login successful", "profile" => "Customer");
                }
            } else {
                $result = $this->Customers_model->user_login($_POST);
                if (!empty($result)) {
                    $completed = true;
                    if ($result->adr_id == null) {
                        $result->sbrb_deliveryfreelimit = 0;
                        $completed = false;
                    }
                    $profiles[] = array("completed" => $completed, "content" => $result, "message" => "Login successful", "profile" => "Customer");
                } else {
                    $message = "User and password combination is incorrect";
                }
            }

            //Check if it is a Driver
            $this->load->model("Drivers_model");
            $checkIfDriverExists = $this->Drivers_model->getDriverByEmail($_POST["cust_emailaddress"]);
            if (!empty($checkIfDriverExists)) {
                $verifyPassword = $this->Drivers_model->login($_POST["cust_password"], $_POST["cust_emailaddress"]);
                if (!empty($verifyPassword)) {
                    $profiles[] = array("completed" => true, "content" => $verifyPassword, "message" => "Logged as Driver", "profile" => "Driver");
                } else {
                    $message = "User and password combination is incorrect";
                }
            }

            //Check if it is a Manager
            $this->load->model("User_model");
            $checkIfManageExists = $this->User_model->getUserByEmail($_POST["cust_emailaddress"]);

            if (!empty($checkIfManageExists)) {
                $verifyManager = $this->User_model->getUserLoggingApi($_POST["cust_emailaddress"], $_POST["cust_password"]);
                if (!empty($verifyManager)) {
                    $verifyManager->cust_id = 0;
                    $profiles[] = array("completed" => true, "content" => $verifyManager, "message" => "Logged as Manager", "profile" => "Manager");
                } else {
                    $message = "User and password combination is incorrect";
                }
            }

            if (!empty($profiles)) {
                $success = true;
                $message = "Login successful";
            } else {
                $success = false;
                if (strlen($message) == 0) {
                    $message = "The email address is not registered";
                }
            }

            $response = array("success" => $success, "message" => $message, "content" => $profiles);
        } else {
            $response = $this->incompleteParametersMessage();
        }
        $response = json_encode($response, true);
        print $response;
    }

    function loginFacebook() {
        if (isset($_POST['key']) && $_POST['key'] == $this->getKey() && isset($_POST["cust_emailaddress"])
            && isset($_POST["facebook"])) {

            $this->load->model("Customers_model");
            $login = $this->Customers_model->loginFacebook($_POST);
            $response = $login;

            unset($_POST["facebook"]);
            $_POST["cust_restid"] = 6;
        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }
        $response = json_encode($response);
        print $response;
    }

    function register() {
        if (isset($_POST['key']) && $_POST['key'] == '05130d60b3bee9339c7b4fb9e6fc83b6' && isset($_POST["cust_emailaddress"])
            && isset($_POST["cust_password"]) && isset($_POST["cust_mobphone"]) && isset($_POST["adr_suburb"])
            && isset($_POST["adr_streetname"]) && isset($_POST["adr_streetnumber"]) && isset($_POST["cust_id"])) {

            $this->load->model("Customers_model");
            //->check email exists
            $checkEmail = $this->Customers_model->checkIfEmailExists($_POST["cust_emailaddress"]);
            if ($_POST["cust_id"] == 0) {
                if (!empty($checkEmail) && $checkEmail->cust_allowloginmob == 1 && strlen($checkEmail->cust_password) > 0 && $checkEmail->cust_isdeleted == 0) {
                    $return = array("success" => false, "message" => "OOPS you have already registered before with ".$_POST["cust_emailaddress"].".\nPlease use the password reminder link on the login screen.");
                    $return = json_encode($return, true);
                    print $return;
                    die();
                }

                if ((!empty($checkEmail) && $checkEmail->cust_allowloginmob == 0) || (!empty($checkEmail) && strlen($checkEmail->cust_password) == 0) || (!empty($checkEmail) && $checkEmail->cust_isdeleted == 1)) {
                    $_POST["cust_id"] = $checkEmail->cust_id;
                    $_POST["cust_allowloginmob"] = 1;
                    $_POST["cust_isdeleted"] = 0;
                }
            }

            //->check if phone exists
            if ($_POST["cust_id"] == 0) {
                $checkMobile = $this->Customers_model->checkMobileNumberExists($_POST["cust_mobphone"]);
                if (!empty($checkMobile) && $checkMobile->cust_allowloginmob == 1 && $checkMobile->cust_emailaddress != $_POST["cust_emailaddress"] && strlen($checkMobile->cust_password) > 0 && $checkEmail->cust_isdeleted == 0) {
                    $return = array("success" => false, "message" => "The mobile number you are trying to register already exists in our records with a different email address.\nIf you need further assistance to complete your registration, please contact us.");
                    $return = json_encode($return, true);
                    print $return;
                    die();
                }

                if ((!empty($checkMobile) && $checkMobile->cust_allowloginmob == 0) || (!empty($checkMobile) && !empty($checkEmail))) {
                    $_POST["cust_id"] = $checkMobile->cust_id;
                    $_POST["cust_allowloginmob"] = 1;
                    $_POST["cust_isdeleted"] = 0;
                }
            }

            $_POST["cust_isdeleted"]  = 0;
            $_POST["cust_createdate"] = date("Y-m-d H:i:s");
            $_POST["cust_ismember"]   = 1;

            //-> Create Customer Address
            $addressArray = array(
                "adr_shortname"     => "Added from mobile",
                "adr_suburb"        => $_POST["adr_suburb"],
                "adr_streetname"    => $_POST["adr_streetname"],
                "adr_unitnumber"    => $_POST["adr_unitnumber"],
                "adr_streetnumber"  => $_POST["adr_streetnumber"],
                "adr_neareststreet" => $_POST["adr_neareststreet"],
                "adr_id"            => $_POST["adr_id"],
                "adr_setasdefault"  => 1,
                "adr_isactive"      => 1
            );

            //-> delete address field from POST var
            unset($_POST["adr_shortname"]);
            unset($_POST["adr_suburb"]);
            unset($_POST["adr_streetname"]);
            unset($_POST["adr_unitnumber"]);
            unset($_POST["adr_streetnumber"]);
            unset($_POST["adr_neareststreet"]);
            unset($_POST["adr_id"]);
            unset($_POST["key"]);

            if (strlen($_POST["cust_password"]) == 0) {
                unset($_POST["cust_password"]);
            }

            //->Register the customer
            $response = $this->Customers_model->register($_POST, $addressArray);

        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }

        $response = json_encode($response, true);
        print $response;
    }

    function getCustomerProfile() {
        if (isset($_GET['key']) && $_GET['key'] == '05130d60b3bee9339c7b4fb9e6fc83b6' && isset($_GET["customer_id"])) {
            $this->load->model("Customers_model");

            $customer = $this->Customers_model->getDetails($_GET["customer_id"]);
            if (!empty($customer)) {
                $response = array("success" => true, "content" => $customer);
            } else {
                $response = array("success" => false, "message" => "We can't find any details with the ID provided");
            }
        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }

        $response = json_encode($response);
        print $response;
    }

    function registerDeviceToken() {
        if (isset($_POST['key']) && $_POST['key'] == '05130d60b3bee9339c7b4fb9e6fc83b6' && isset($_POST["customer_id"]) && isset($_POST["token"])
            && isset($_POST["os"])) {

            unset($_POST["key"]);

            $today = date("Y-m-d H:i:s");
            $_POST["updated_at"] = $today;

            $this->load->model("Device_model");
            $record = $this->Device_model->getDeviceByUserOs($_POST["customer_id"], $_POST["os"]);

            if (strlen($_POST["customer_id"]) == 0) {
                unset($_POST["customer_id"]);
            }

            if (!empty($record)) {
                if (strcmp($record->token, $_POST["token"]) == 0) {
                    $response = array("success" => false, "message" => "Token already exists");
                } else {
                    $this->Device_model->update($_POST);
                    $response = array("success" => true, "message" => "Token updated");
                }
            } else {
                $_POST["created_at"] = $today;
                $this->Device_model->create($_POST);
                $response = array("success" => true, "message" => "Token stored");
            }
        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }

        $response = json_encode($response);
        print $response;
    }


    /**
     * Check if the user make an order for the first time using the mobile App
     */
    function checkIfOrderFirstTime() {
        if (isset($_GET['key']) && $_GET['key'] == '05130d60b3bee9339c7b4fb9e6fc83b6' && isset($_GET["customer_id"]) && isset($_GET["amount"])
            && isset($_GET["token"])) {
            $token = $_GET["token"];
            unset($_GET["token"]);
            $this->load->model("Orders_model");
            $this->load->model("Orderdevice_model");
            if ($_GET["amount"] > 0 ) {
                $orders      = $this->Orders_model->getOrderByCustomerMobile($_GET["customer_id"]);
                $userBought  = $this->Orderdevice_model->getRecorByCustomer($_GET["customer_id"]);
                $tokenBought = $this->Orderdevice_model->getRecordByToken($token);

                if (/*!empty($orders) ||*/ !empty($userBought) || !empty($tokenBought)) {
                    $response = array("success" => false, "message" => "");
                } else {
                    $this->load->model("Appsettings_model");
                    $settings = $this->Appsettings_model->getActiveDiscount($_GET["amount"]);
                    if (!empty($settings)) {
                        $percAmount = ($_GET["amount"] * $settings->first_time_percentage) / 100;
                        if ($percAmount < $settings->first_time_amount) {
                            $percAmount = $settings->first_time_amount;
                        }
                        $percAmount = $percAmount * 1;
                        $response = array("success" => true, "amount" => $percAmount);
                    } else {
                        $response = array("success" => false, "message" => "");
                    }
                }
            } else {
                $response = array("success" => false, "message" => "");
            }
        } else {
            $response = array("success" => false, "message" => "Wrong restaurant parameters received");
        }
        $response = json_encode($response);
        print $response;
    }

    function getFirstTimeOrderMessage() {
        if (isset($_GET['key']) && $_GET['key'] == '05130d60b3bee9339c7b4fb9e6fc83b6') {
            $this->load->model("Appsettings_model");
            $discount = $this->Appsettings_model->checkActive();
            if (!empty($discount)) {
                $response = array("success" => true, "message" => $discount->initial_message);
            } else {
                $response = array("success" => false, "message" => "");
            }
        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }

        $response = json_encode($response);
        print $response;

    }
    //******* END USER METHODS

    //******* GENERAL SETTINGS FROM THE WEBSITE
    function getWebSettings() {
        if (isset($_GET['key']) && $_GET['key'] == '05130d60b3bee9339c7b4fb9e6fc83b6' && isset($_GET["restaurant_id"])) {
            $restaurant = $_GET["restaurant_id"];
            $this->load->model("Websettings_model");
            $webSettings = $this->Websettings_model->getSettings($restaurant);
            if (!empty($webSettings)) {
                $response = array("success" => true, "content" => $webSettings);
            } else {
                $response = array("success" => false, "message" => "Wrong restaurant parameters received");
            }
        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }

        $response = json_encode($response);
        print $response;
    }

    function getPolicy() {
        if (isset($_GET['key']) && $_GET['key'] == '05130d60b3bee9339c7b4fb9e6fc83b6' && isset($_GET["restaurant_id"])) {

            $this->load->model("Appsettings_model");
            $policy = $this->Appsettings_model->getAll();

            $response = array("success" => true, "content" => $policy);

        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }

        $response = json_encode($response);
        print $response;
    }

    function sendContactForm() {
        if (isset($_POST['key']) && $_POST['key'] == '05130d60b3bee9339c7b4fb9e6fc83b6' && isset($_POST["name"])
            && isset($_POST["phone"]) && isset($_POST["email"]) && isset($_POST["comment"])) {

            $this->load->model("Email_model");
            $body = $this->Email_model->contactBody($_POST);

            require 'assets/includes/email/PHPMailerAutoload.php';
            $this->sendEmail("info@littlepiazza.com.au", 'Message from the App', $body);
            $response = array("success" => true, "message" => "Thanks for contact us. Your message has been sent.");
        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }

        $response = json_encode($response);
        print $response;
    }

    function sendReservationForm() {

        if (isset($_POST['key']) && $_POST['key'] == $this->getKey() ) {

            unset($_POST["key"]);

            $this->load->model("Booking_model");
            $data = array(
                "tbl_restid"    => 6,
                "tbl_servicetype"   => 3,
                "tbl_datetime"      => $_POST["date"]." ".$_POST["time"],
                "tbl_firstname"     => $_POST["name"],
                "tbl_lastname"      => "",
                "tbl_mobilenumber"  => $_POST["mobile"],
                "tbl_email"         => $_POST["email"],
                "tbl_numberofguest" => $_POST["quests"],
                "tbl_specialrequest"=> $_POST["special_request"],
                "tbl_bookingname"   => ""
            );
            $this->Booking_model->storeBooking($data);

            $message = $this->Booking_model->bookingBody($_POST);

            require 'assets/includes/email/PHPMailerAutoload.php';
            $this->sendEmail('info@littlepiazza.com.au','Booking Request', $message);

            $device = (isset($_POST["device"]) ? $_POST["device"] : "Mobile App");
            $date = date("M d Y", strtotime($_POST["date"]));
            $time = date("H:i:s", strtotime($_POST["time"]));

            $this->load->model("Websettings_model");
            $settings = $this->Websettings_model->getSettings(6);
            $number = $settings->web_staffnotifysms;
            $reservationName = (isset($_POST["name"]) && isset($_POST["surname"]) ? $_POST["name"].' '.$_POST["surname"] : $_POST["name"]);
            $message = $reservationName.' just made a booking from '.$device.' for '.$_POST["quests"].' for '.$date.' '.$time.'';

//            $this->sendSMS($settings->web_staffnotifysms, $_POST["name"].' '.$_POST["surname"].' just made a booking from '.$device.' for '.$_POST["quests"].' for '.$date.' '.$time.'');

            if ($_POST["device"] == "iOS App") {
                $answer = $this->Booking_model->bookingResponse($_POST);
                $this->sendEmail($_POST["email"], 'Booking from The App', $answer);
            }

            $response = array("success" => true, "message" => "Thank you. You will receive an email shortly to confirm your booking.", "content" => $message, "number" => $number);
        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }

        $response = json_encode($response);
        print $response;
    }

    function sendReservationFormConfirm() {
        unset($_POST["key"]);
        $this->load->model("Booking_model");
        $answer = $this->Booking_model->bookingResponse($_POST);

        require 'assets/includes/email/PHPMailerAutoload.php';
        $this->sendEmail($_POST["email"], 'Booking from The App', $answer);
        $response = array("success" => true);
        $response = json_encode($response, true);
        print $response;
    }


    //******* END GENERAL SETTINGS FROM THE WEBSITE

    //******* CATEGORIES
    function getCategoriesDelivery() {
        if (isset($_GET['key']) && $_GET['key'] == $this->getKey() && isset($_GET["half"])) {
            $this->load->model("Category_model");

            if ($_GET["half"] === 'false' || $_GET["half"] === '0') {
                $categories = $this->Category_model->getActiveDeliveryRecords();
            } else {
                $categories = $this->Category_model->getCategoriesWithHalf();
            }

            $this->load->model("CategoryPicture_model");
            foreach ($categories as $key => $value) {
                $tiName = str_replace(" ", "_", trim($value->cat_name));
                $tiName = str_replace("'", "", trim($tiName));
                $value->image_name =  strtolower($tiName);
                $picture = $this->CategoryPicture_model->getRecordByCategory($value->cat_id);
                $photo = (isset($picture->photo) ? $picture->photo : "");
                $value->picture    = "https://ik.imagekit.io/ztrzsxyjq/assets/img/categories/".$photo;
            }
            if (!empty($categories)) {
                $response = array("success" => true, "content" => $categories);
            } else {
                $response = array("success" => false, "message" => "No content found for categories");
            }
        } else {
            $response = $this->incompleteParametersMessage();
        }

        $this->sendResponse($response);
    }
    //*******

    //******* PRODUCTS
    function getProductsWithHalf() {
        if (isset($_GET['key']) && $_GET['key'] == $this->getKey()) {
            $this->load->model("Product_model");
            $products = $this->Product_model->getProductsWithHalf();
            if (!empty($products)) {
                $this->load->model("Productallows_model");
                foreach ($products as $key => $value) {
                    $value->mobile = base_url("assets/img/products/mobile/".$value->picture);
                    $value->picture = base_url("assets/img/products/".$value->picture);

                    //Check Options for ordering
                    $value->base_types  = $this->Productallows_model->getProductBaseTypes($value->prd_id);
                    if (count($value->base_types) > 0) {
                        $i = 0;
                        foreach ($value->base_types as $ky => $val) {
                            if ($i == 0) {
                                $val->selected = true;
                            } else {
                                $val->selected = false;
                            }
                            $i++;
                        }
                    }
                    $value->sizes       = $this->checkSizesAvailable($value->prd_smallprice, $value->prd_mediumprice, $value->prd_largeprice);
                    $value->add_top     = $this->Productallows_model->getProductToppings($value->prd_id, "A");
                    $value->remove_top  = $this->Productallows_model->getProductToppings($value->prd_id, "R");
                    $value->sauces      = $this->Productallows_model->getProductSauces($value->prd_id);
                }
                $response = array("success" => true, "content" => $products);
            } else {
                $response = array("success" => false, "message" => "No content found for this category");
            }
        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }

        $response = json_encode($response);
        print $response;
    }

    function comparePrices() {
        if (isset($_GET['key']) && $_GET['key'] == $this->getKey() && isset($_GET["product_one"]) && isset($_GET["product_two"])
            && isset($_GET["size"])) {
            $this->load->model("Product_model");

            $price1 = $this->Product_model->getPriceProductSize($_GET["product_one"], strtolower($_GET["size"]));
            $price2 = $this->Product_model->getPriceProductSize($_GET["product_two"], strtolower($_GET["size"]));

            if ($price1->price >= $price2->price) {
                $price = 0;
                $price_product = 0;
            } else {
                $price_product = $price2->price * 1;
                $price = $price2->price - $price1->price;
            }

            $response = array("success" => true, "price" => $price, "product_price" => $price_product);

        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }

        $response = json_encode($response);
        print $response;
    }

    function getProductDetail() {
        if (isset($_GET['key']) && $_GET['key'] == $this->getKey() && isset($_GET["product_id"])) {
            $this->load->model("Product_model");
            $product = $this->Product_model->getRecord($_GET["product_id"]);
            if (!empty($product)) {
                $this->load->model("Productallows_model");
                //foreach ($product as $key => $value) {
                    $product->mobile = base_url("assets/img/products/mobile/".$product->picture);
                    $product->picture = base_url("assets/img/products/".$product->picture);

                $this->load->model("Productallowed_model");
                    //Check Options for ordering
                    $product->base_types  = $this->Productallows_model->getProductBaseTypes($product->prd_id);
                    if (count($product->base_types) > 0) {
                        $i = 0;
                        foreach ($product->base_types as $ky => $val) {
                            if ($i == 0) {
                                $val->selected = true;
                            } else {
                                $val->selected = false;
                            }
                            $i++;
                        }
                    }
                    $product->options     = $this->Productallowed_model->getOptionsByProduct($product->prd_id);
                    $product->sizes       = $this->checkSizesAvailable($product->prd_smallprice, $product->prd_mediumprice, $product->prd_largeprice);
                    $product->add_top     = $this->Productallows_model->getProductToppings($product->prd_id, "A");
                    $product->remove_top  = $this->Productallows_model->getProductToppings($product->prd_id, "R");
                    $product->sauces      = $this->Productallows_model->getProductSauces($product->prd_id);
                //}
                $response = array("success" => true, "content" => $product);
            } else {
                $response = array("success" => false, "message" => "No content found for this category");
            }
        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }

        $response = json_encode($response);
        print $response;
    }

    function getProductsCategories() {
        if (isset($_GET['key']) && $_GET['key'] == $this->getKey() && isset($_GET["category"])) {
            $this->load->model("Product_model");
            $products = $this->Product_model->getProductsByCategory($_GET["category"]);
            if (!empty($products)) {
                $this->load->model("Productallows_model");
                $this->load->model("Productallowed_model");
                foreach ($products as $key => $value) {
                    $value->mobile = base_url("assets/img/products/mobile/".$value->picture);
                    $value->picture = base_url("assets/img/products/".$value->picture);

                    //Check Options for ordering
                    $value->base_types  = $this->Productallows_model->getProductBaseTypes($value->prd_id);
                    if (count($value->base_types) > 0) {
                        $i = 0;
                        foreach ($value->base_types as $ky => $val) {
                            if ($i == 0) {
                                $val->selected = true;
                            } else {
                                $val->selected = false;
                            }
                            $i++;
                        }
                    }
                    $value->options     = $this->Productallowed_model->getOptionsByProduct($value->prd_id);
                    $value->sizes       = $this->checkSizesAvailable($value->prd_smallprice, $value->prd_mediumprice, $value->prd_largeprice);
                    $value->add_top     = $this->Productallows_model->getProductToppings($value->prd_id, "A");
                    $value->remove_top  = $this->Productallows_model->getProductToppings($value->prd_id, "R");
                    $value->sauces      = $this->Productallows_model->getProductSauces($value->prd_id);
                }
                $response = array("success" => true, "content" => $products);
            } else {
                $response = array("success" => false, "message" => "No content found for this category");
            }
        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }

        $response = json_encode($response);
        print $response;
    }

    /**
     * Send back all options for a product when is ordered (Add, Remove, Base Type, Sauces)
     */
    function getProductOptions() {
        if (isset($_GET['key']) && $_GET['key'] == $this->getKey() && isset($_GET["product_id"])) {
            $product_id = $_GET["product_id"];
            $this->load->model("Productallows_model");
            $bases  = $this->Productallows_model->getProductBaseTypes($product_id);
            $addTop = $this->Productallows_model->getProductToppings($product_id, "A");
            $remTop = $this->Productallows_model->getProductToppings($product_id, "R");
            $sauces = $this->Productallows_model->getProductSauces($product_id);

            $response = array("success" => true, "bases" => $bases);

        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }

        $response = json_encode($response);
        print $response;
    }

    function getAllDelivery() {
        if (isset($_GET['key']) && $_GET['key'] == $this->getKey()) {
            $this->load->model("Product_model");
            $products = $this->Product_model->getAllDelivery();
            if (!empty($products)) {
                $this->load->model("Productallows_model");
                foreach ($products as $key => $value) {
                    $value->prd_name = trim($value->prd_name);
                    $value->mobile   = base_url("assets/img/products/mobile/".$value->picture);
                    $value->picture  = base_url("assets/img/products/".$value->picture);

                    //Check Options for ordering
                    $value->base_types  = $this->Productallows_model->getProductBaseTypes($value->prd_id);
                    if (count($value->base_types) > 0) {
                        $i = 0;
                        foreach ($value->base_types as $ky => $val) {
                            if ($i == 0) {
                                $val->selected = true;
                            } else {
                                $val->selected = false;
                            }
                            $i++;
                        }
                    }
                    $value->sizes       = $this->checkSizesAvailable($value->prd_smallprice, $value->prd_mediumprice, $value->prd_largeprice);
                    $value->add_top     = $this->Productallows_model->getProductToppings($value->prd_id, "A");
                    $value->remove_top  = $this->Productallows_model->getProductToppings($value->prd_id, "R");
                    $value->sauces      = $this->Productallows_model->getProductSauces($value->prd_id);
                }
                $response = array("success" => true, "content" => $products);
            } else {
                $response = array("success" => false, "message" => "No content found for this category");
            }
        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }

        $response = json_encode($response);
        print $response;
    }
    //*******

    //*** VOUCHERS - DISCOUNTS
    function validateVoucher() {
        if (isset($_POST['key']) && $_POST['key'] == '05130d60b3bee9339c7b4fb9e6fc83b6' && isset($_POST["customer_id"])
            && isset($_POST["voucher"])) {

            $this->load->model("Customerdiscount_model");
            $discount = $this->Customerdiscount_model->getDiscount($_POST["voucher"]);
            if (!empty($discount)) {
                $amount     = 0;
                $fixed      = false;
                $percentage = false;
                if ($discount->disc_dicountfixed > 0) {
                    $fixed  = true;
                    $amount = $discount->disc_dicountfixed;
                } elseif ($discount->disc_percentage) {
                    $percentage = true;
                    $amount     = $discount->disc_percentage;
                }
                if ($fixed) {
                    $message = "You just redeem a cupon for $".$amount." on your order";
                } else {
                    $message = "You just redeem a cupon for ".$amount."% on your order";
                }
                $response = array("success" => true, "fixed" => $fixed, "percentage" => $percentage, "amount" => $amount, "message" => $message);
            } else {
                $response = array("success" => false, "message" => "The code you entered is not valid");
            }

        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }
        $response = json_encode($response);
        print $response;
    }

    /**
     * Verify if any special discount is configured
     * @params
     * key
     * amount
     * id products array
     * @response
     * JSON with al discounts received in an array at content value
     */
    function checkForDiscounts() {
        if (isset($_GET["key"]) && $_GET["key"] == $this->getKey() && isset($_GET["amount"]) && isset($_GET["products"])) {

            //Disabling the discounts
            $response = array("success" => false, "message" => "");
            $this->sendResponse($response);
            die();

            if (isset($_GET["ord_deliverytype"]) && $_GET["ord_deliverytype"] != 2) {
                $response = $this->incompleteParametersMessage();
                $response = json_encode($response, true);
                print $response;
                die();
            }

            $amount         = $_GET["amount"] * 1;
            $productsArray  = $_GET["products"];

            switch (true) {
                case $amount > 49 && $amount < 99.99:
                    $discount = 5;
                    $message  = "You just received a $5 discount";
                    $name     = "$5 discount";
                    break;
                case $amount > 99.99:
                    $discount = 10;
                    $message  = "You just received a $10 discount";
                    $name     = "$10 discount";
                    break;
                default:
                    $discount = 0;
                    $message  = '';
                    $name     = '';
            }

            if ($discount > 0) {
                $discountArray[] = array("amount" => $discount, "name" => $name, "coupon" => "", "percentage" => 0);
            }

            if (!empty($productsArray) && $amount > 49) {
                $this->load->model("Product_model");
                $product = $this->Product_model->getProductByArrayIdByCategory($productsArray, 2);
                if (!empty($product)) {
                    $discountOffer = $product->prd_fixedprice / 2;
                    $message .= "\nYou are receiving 50% discount on " . $product->prd_name . "";
                    $discountArray[] = array("amount" => $discountOffer, "name" => "Half price gelato offer", "coupon" => "", "percentage" => 0);
                }
            }

            if (isset($discountArray) && !empty($discountArray)) {
                $response = array("success" => true, "message" => $message, "content" => $discountArray);
            } else {
                $response = array("success" => false, "message" => "");
            }

        } else {
            $response = $this->incompleteParametersMessage();
        }

        $response = json_encode($response, true);
        print $response;
    }

    //*******

    //*** PAYMENT PROCESS
    function getPaymentFee() {
        if (isset($_POST['key']) && $_POST['key'] == $this->getKey() && isset($_POST["vendor"])) {
            $this->load->model("Paymentoption_model");
            $provider = $this->Paymentoption_model->getByProvider($_POST["vendor"]);
            if (!empty($provider)) {
                $amount = ($_POST["amount"] * $provider->pay_percentagefee) / 100;
                $response = array("success" => true, "amount" => $amount, "provider" => $provider->pymt_providername);
            } else {
                $response = array("success" => false, "amount" => 0);
            }
        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }
        $response = json_encode($response);
        print $response;
    }

    function processMWPayment() {
        if (isset($_POST['key']) && $_POST['key'] == '05130d60b3bee9339c7b4fb9e6fc83b6' && isset($_POST["total_amount"])
            && isset($_POST["product"]) && isset($_POST["customer_name"]) && isset($_POST["country"])
            && isset($_POST["state"]) && isset($_POST["city"]) && isset($_POST["address"]) && isset($_POST["postcode"])
            && isset($_POST["phone"]) && isset($_POST["customer_ip"]) && isset($_POST["card_number"]) && isset($_POST["security_code"])
            && isset($_POST["card_month_expire"]) && isset($_POST["card_year_expire"]) && isset($_POST["order_items"])
            && isset($_POST["customer_email"])) {

            $this->load->model("Paymentoption_model");
            $paymentSetting = $this->Paymentoption_model->getById(1);

            // Setup your unique authentication data
            define('MW_MERCHANT_UUID', $paymentSetting->pay_merchantuid);
            define('MW_API_KEY', $paymentSetting->pay_apikey);
            define('MW_API_PASS_PHRASE', $paymentSetting->pay_apipasspharse);

            //Payment Data
            $postData['method']                     = 'processCard';
            $postData['merchantUUID']               = MW_MERCHANT_UUID;
            $postData['apiKey']                     = MW_API_KEY;
            $postData['transactionAmount']          = $_POST["total_amount"];
            $postData['transactionCurrency']        = 'aud';
            $postData['transactionProduct']         = $_POST["product"];
            $postData['customerName']               = $_POST["customer_name"];
            $postData['customerCountry']            = $_POST["country"];
            $postData['customerState']              = $_POST["state"];
            $postData['customerCity']               = $_POST["city"];
            $postData['customerAddress']            = $_POST["address"];
            $postData['customerPostCode']           = $_POST["postcode"];
            $postData['customerPhone']              = $_POST["phone"];
            $postData['customerEmail']              = $_POST["customer_email"];
            $postData['customerIP']                 = $_POST["customer_ip"];
            $postData['paymentCardNumber']          = $_POST["card_number"];
            $postData['paymentCardName']            = $_POST["customer_name"];
            $postData['paymentCardExpiry']          = $_POST["card_month_expire"].$_POST["card_year_expire"];
            $postData['paymentCardCSC']             = $_POST["security_code"];
            $postData['hash'] = $this->Paymentoption_model->calculateHash($postData, MW_API_PASS_PHRASE, MW_MERCHANT_UUID);


            $paymentResponse = $this->Paymentoption_model->processPayment($postData);

            if ($paymentResponse["status"]) {
                $response = array("success" => true, "message" => "En desarrollo aun");
            } else {
                $response = array("success" => true, "message" => "Thanks for your order");
            }


            //$response = array("success" => true, "message" => "En desarrollo aun");


        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }
        $response = json_encode($response);
        print $response;
    }

    function processPaypalPaymentiOS() {
        if (isset($_POST['key']) && $_POST['key'] == $this->getKey() && isset($_POST["total_amount"])
            && isset($_POST["products"]) && isset($_POST["customer_ip"]) && isset($_POST["order_items"])
            && isset($_POST["ord_authorizationcode"])) {


//            $myfile = fopen("assets/newfile.txt", "w") or die("Unable to open file!");
//            fwrite($myfile, serialize($_POST));
//            fclose($myfile);

            //check if the user has a mail address
            $this->load->model("Customeraddress_model");
            $addr = $this->Customeraddress_model->getAddressByCustomer($_POST["ord_customerid"]);
            if (empty($addr)) {
                $addrResponse = array("success" => false, "message" => "Please update your profile at the Profile Screen before continue");
                $addrResponse = json_encode($addrResponse, true);
                print $addrResponse;
                die();
            }


            if (strcmp($_POST["ord_from"], "And_App") == 0) {
                $orderNumber = 'AND-'.$this->getOrderCodeNumber();
            } else {
                $orderNumber = 'iOS-'.$this->getOrderCodeNumber();
            }

            $devTime = (strlen($_POST["dev_time"]) > 0 ? $_POST["dev_time"] : date("Y-m-d", strtotime($_POST["dev_time"]."+ 30 minutes")));

            //create Order Data
            $timezone = 'Australia/Sydney';  //perl: $timeZoneName = "MY TIME ZONE HERE";
            $date = new DateTime('now', new DateTimeZone($timezone));
            $_POST["ord_createdate"] = $date->format('Y-m-d H:i:s');

            $deliveryTpe = $this->getDeliveryOption($_POST["ord_from"], $_POST["ord_deliverytype"], $_POST["ord_deliverytime"]);

            $dataOrder = array(
                "ord_number"                => $orderNumber,
                "ord_customerid"            => $_POST["ord_customerid"],
                "ord_customeraddressid"     => $_POST["ord_customeraddressid"],
                "ord_restid"                => 6,
                "ord_deliveryoption"        => $_POST["ord_deliveryoption"],
                "ord_deliverytime"          => (strlen($_POST["ord_deliverytime"]) > 10 ? $_POST["ord_deliverytime"] : Null),
                "ord_deliverytype"          => $deliveryTpe, //($_POST["ord_deliverytype"]),
                "ord_paymenttype"           => $_POST["ord_paymenttype"],
                "ord_subtotal"              => $_POST["ord_subtotal"],
                "ord_deliveryfee"           => $_POST["ord_deliveryfee"],
                "ord_discounttotal"         => $_POST["ord_discounttotal"],
                "ord_grosstotal"            => $_POST["total_amount"], //($_POST["ord_subtotal"] + $_POST["ord_deliveryfee"] + $_POST["ord_paymentfee"]) - $_POST["ord_discounttotal"],
                "ord_paymentfee"            => $_POST["ord_paymentfee"],
                "ord_authorizationcode"     => $_POST["ord_authorizationcode"],
                "ord_transcationcode"       => $_POST["ord_authorizationcode"],
                //"ord_creditcardno"          => $_POST["ord_creditcardno"],
                "ord_from"                  => $_POST["ord_from"],
                "ord_commentcust"           => $_POST["ord_commentcust"],
                "ord_createdate"            => $_POST["ord_createdate"],
                "ord_createdip"             => $_POST["ord_createdip"],
                "ord_cancelreason"          => ""
            );

            //Save order main record tbl_orders
            $this->load->model("Orders_model");
            $newOrder = $this->Orders_model->saveOrder($dataOrder);

            $isFirstTime = (isset($_POST["isFirstTime"]) ? $_POST["isFirstTime"] : 0);
            $discountTotal = $_POST["ord_discounttotal"] * 1;
            if ($discountTotal > 0) {
                $this->storeDiscount($_POST["ord_from"], $_POST["ord_customerid"], $_POST["ord_createdate"], $newOrder, $isFirstTime);
            }

            //Save order details
            $products = json_decode($_POST["products"]);
            $this->load->model("Orderitems_model");
            $this->load->model("Ordertopping_model");

            foreach ($products as $key => $value) {
                $dataDetails= array(
                    "ori_restid"            => 6,
                    "ori_orderid"           => $newOrder,
                    "ori_prodid"            => $value->ori_prodid,
                    "ori_price"             => $value->individual_price,
                    "ori_usedpricelevel"    => $value->ori_usdpricelevel,
                    "ori_quantity"          => $value->ori_quantity,
                    "ori_basetype"          => $value->ori_basetype,
                    "ori_halfandhalf"       => $value->ori_halfandhalf,
                    "ori_halfandhalfwith"   => (isset($value->ori_halfandhalfwith) && $value->ori_halfandhalfwith > 0 ? $value->ori_halfandhalfwith : NULL),
                    "ori_sizeselection"     => (strcmp($value->ori_sizeselection, "Small") == 0 ? $value->ori_sizeselection : '')
                );
                $itemOrdered = $this->Orderitems_model->saveOrder($dataDetails);

                $extraTopping = (isset($value->extras) ? $value->extras : $value->topping);
                foreach ($extraTopping as $keyx => $valuex) {
                    $topping = array(
                        "oro_restid" => 6,
                        "oro_orderdetailid" => $itemOrdered,
                        "oro_productid" => $valuex->oro_productid,
                        "oro_optype" => $valuex->oro_optype,
                        "oro_name" => $valuex->oro_name,
                        "oro_price" => (strcmp($valuex->oro_optype, "R") == 0 ? 0 : $valuex->oro_price)
                    );
                    $this->Ordertopping_model->saveOrder($topping);

                    if (strcmp($valuex->oro_optype, "O") == 0) {
                        $dataType = array(
                            "ori_selectedoption" => $valuex->oro_name
                        );
                        $this->Orderitems_model->updateType($itemOrdered, $dataType);
                    }
                }

            }

            $backMessage = ($_POST["ord_deliverytype"] == 1 ? "DELIVERY" : "PICKUP");
            if (strcmp($backMessage, "DELIVERY") == 0) {
                $devTime = '';
            }
            $bodyMessage = "Your ".$backMessage." Order has been successfully Processed. \n \n Very soon, You will receive an SMS from us regarding, your ".$backMessage." Time. A copy of your tax invoice will also be sent to your email address. \n \n PLS. Contact us if you DO NOT receive a confirmation SMS from us within 15 minutes.";

            $this->load->model("Paymentprovider_model");
            $provider = $this->Paymentprovider_model->getRecord($_POST["ord_paymenttype"]);

            $orderDetails = $this->Orders_model->getOrderDetailsByID($newOrder);

            switch ($provider->pay_id) {
                case 1:
                    $detailPaymentTool = $orderDetails->ord_creditcardno;
                    break;
                case 2:
                    $detailPaymentTool = $_POST["customer_email"];
                    break;
                default:
                    $detailPaymentTool = "";
            }

            $this->load->model("Customers_model");
            $customer = $this->Customers_model->getRecordById($_POST["customer_id"]);
            $custAddress = $customer->adr_unitnumber.'/'.$customer->adr_streetnumber.' '.$customer->sbrc_streetname.', '.$customer->sbrb_name;

            $emailBody = $this->createEmailConfirmation($orderNumber, $customer->cust_name.' '.$customer->cust_surname, $customer->cust_mobphone, $custAddress, $products, $_POST["ord_deliveryfee"], $_POST["ord_paymentfee"], $_POST["ord_discounttotal"], $_POST["ord_createdate"], $devTime, $provider->pay_shortname, $_POST["ord_deliveryoption"], $_POST["ord_deliverytype"], $detailPaymentTool, $_POST["ord_subtotal"], $_POST["total_amount"], $_POST["ord_commentcust"]);

            require 'assets/includes/email/PHPMailerAutoload.php';
            $this->sendEmail($_POST["customer_email"], 'Order Confirmation: '.$orderNumber, $emailBody);

//            $smsMessage = "Your ".$backMessage." Order has been successfully Processed. Very soon, You will receive an
//                SMS from us regarding, your Pickup Time. A copy of your tax invoice will also be sent to your email address. PLS.
//                Contact us if you DO NOT receive a confirmation SMS from us within 15 minutes.";
//            $this->sendSMS($customer->cust_mobphone, $smsMessage);

            $response = array("success" => true, "data" => $dataOrder ,"message" => $bodyMessage);

        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }
        $response = json_encode($response);
        print $response;
    }

    function getAddressUser() {
        $this->load->model("Customers_model");
        $customer = $this->Customers_model->getRecordById($_GET["customer_id"]);
        $response = json_encode($customer);
        print $response;
    }


    function processMWPaymentiOS() {
        if (isset($_POST['key']) && $_POST['key'] == $this->getKey() && isset($_POST["total_amount"])
            && isset($_POST["products"]) && isset($_POST["customer_name"]) && isset($_POST["country"])
            && isset($_POST["state"]) && isset($_POST["city"]) && isset($_POST["address"]) && isset($_POST["postcode"])
            && isset($_POST["phone"]) && isset($_POST["customer_ip"]) && isset($_POST["card_number"]) && isset($_POST["security_code"])
            && isset($_POST["card_month_expire"]) && isset($_POST["card_year_expire"]) && isset($_POST["order_items"])
            && isset($_POST["customer_email"])) {

            if (!isset($_POST["ord_paymentfee"])) {
                $response = array(
                    "success" => false,
                    "message" => "Hi we recently encountered a bug in our payment system and we fixed that with a new version. Pls download our latest version to pay for this order"
                );
                $this->sendResponse($response);
                die();
            }

            $orderNumberArray = explode('-', $_POST["ord_number"]);
            if (strcmp($orderNumberArray[0], 'And') == 0) {
                $_POST["ord_number"] = $_POST["ord_number"];
            } else {
                $_POST["ord_number"] = $_POST["ord_number"];
            }

            $this->load->model("Paymentoption_model");
            $paymentSetting = $this->Paymentoption_model->getById(1);

            // Setup your unique authentication data
            define('MW_MERCHANT_UUID', $paymentSetting->pay_merchantuid);
            define('MW_API_KEY', $paymentSetting->pay_apikey);
            define('MW_API_PASS_PHRASE', $paymentSetting->pay_apipasspharse);

            //Payment Datagetproduct
            $postData['method']                     = 'processCard';
            $postData['merchantUUID']               = MW_MERCHANT_UUID;
            $postData['apiKey']                     = MW_API_KEY;
            $postData['transactionAmount']          = $_POST["total_amount"];
            $postData['transactionCurrency']        = 'aud';
            $postData['transactionProduct']         = "Little Piazza Mobile Order";
            $postData['customerName']               = $_POST["customer_name"];
            $postData['customerCountry']            = $_POST["country"];
            $postData['customerState']              = 'Queensland'; //$_POST["state"];
            $postData['customerCity']               = 'Brisbane'; //$_POST["city"];
            $postData['customerAddress']            = $_POST["address"];
            $postData['customerPostCode']           = '4001'; //$_POST["postcode"];
            $postData['customerPhone']              = $_POST["phone"];
            $postData['customerEmail']              = $_POST["customer_email"];
            $postData['customerIP']                 = $_POST["customer_ip"];
            $postData['paymentCardNumber']          = $_POST["card_number"];
            $postData['paymentCardName']            = $_POST["customer_name"];
            $postData['paymentCardExpiry']          = $_POST["card_month_expire"].$_POST["card_year_expire"];
            $postData['paymentCardCSC']             = $_POST["security_code"];
            //$postData['hash'] = md5(strtolower(MW_API_PASS_PHRASE. MW_MERCHANT_UUID. $postData['transactionAmount'] . $postData['transactionCurrency'])); //$this->Paymentoption_model->calculateHash($postData, MW_API_PASS_PHRASE, MW_MERCHANT_UUID);
            $postData['hash'] = md5(strtolower(md5(MW_API_PASS_PHRASE) . MW_MERCHANT_UUID . $postData['transactionAmount'] . $postData['transactionCurrency']));

            $totalAmount   = $_POST["total_amount"];
            $customerEmail = $_POST["customer_email"];
            $isFirstTime   = (isset($_POST["isFirstTime"]) ? $_POST["isFirstTime"] : 0);
            unset($_POST["isFirstTime"]);
            unset($_POST["total_amount"]);
            unset($_POST["customer_name"]);
            unset($_POST["country"]);
            unset($_POST["state"]);
            unset($_POST["city"]);
            unset($_POST["address"]);
            unset($_POST["postcode"]);
            unset($_POST["phone"]);
            unset($_POST["customer_email"]);
            unset($_POST["customer_ip"]);
            unset($_POST["customer_name"]);
            unset($_POST["card_month_expire"]);
            unset($_POST["card_year_expire"]);
            unset($_POST["security_code"]);
            unset($_POST["key"]);

            //$postData['hash'] = calculateHash($postData);

            $paymentResponse = $this->Paymentoption_model->processPayment($postData);

            if ($paymentResponse["responseData"]["responseCode"] == 0 /*|| strcmp($_POST["card_number"], "5123456789012346") == 0*/) {
                $_POST["ord_creditcardno"] = "";
                $cardArray = str_split($_POST["card_number"]);
                for ($e = 0; $e < count($cardArray); $e++) {
                    if ($e >= 3 && $e <= 11) {
                        $cardArray[$e] = ($e % 4 == 0 ? " *" : "*");
                    }
                    $_POST["ord_creditcardno"] = $_POST["ord_creditcardno"].$cardArray[$e];
                }
                $_POST["ord_paymentcompleted"] = 1;

                $timezone = 'Australia/Sydney';  //perl: $timeZoneName = "MY TIME ZONE HERE";
                $date = new DateTime('now', new DateTimeZone($timezone));
                $_POST["ord_createdate"] = $date->format('Y-m-d H:i:s');

                unset($_POST["card_number"]);

                $devTime = (strlen($_POST["dev_time"]) > 0 ? $_POST["dev_time"] : date("Y-m-d", strtotime($_POST["dev_time"]."+ 30 minutes")));

                $deliveryTpe = $this->getDeliveryOption($_POST["ord_from"], $_POST["ord_deliverytype"], $_POST["ord_deliverytime"]);

                //create Order Data
                $dataOrder = array(
                    "ord_number"                => $_POST["ord_number"],
                    "ord_customerid"            => $_POST["ord_customerid"],
                    "ord_customeraddressid"     => $_POST["ord_customeraddressid"],
                    "ord_restid"                => 6,
                    "ord_deliveryoption"        => $_POST["ord_deliveryoption"],
                    "ord_deliverytime"          => (strlen($_POST["ord_deliverytime"]) > 10 ? $_POST["ord_deliverytime"] : Null),
                    "ord_deliverytype"          => $deliveryTpe, //($_POST["ord_deliverytype"]),
                    "ord_paymenttype"           => $_POST["ord_paymenttype"],
                    "ord_subtotal"              => $_POST["ord_subtotal"],
                    "ord_deliveryfee"           => $_POST["ord_deliveryfee"],
                    "ord_discounttotal"         => $_POST["ord_discounttotal"],
                    "ord_grosstotal"            => $postData['transactionAmount'], //($_POST["ord_subtotal"] + $_POST["ord_deliveryfee"] + $_POST["ord_paymentfee"]) - $_POST["ord_discounttotal"],
                    "ord_paymentfee"            => $_POST["ord_paymentfee"],
                    "ord_authorizationcode"     => $paymentResponse["responseData"]["authCode"],
                    "ord_transcationcode"       => $paymentResponse["transactionID"],
                    "ord_creditcardno"          => $_POST["ord_creditcardno"],
                    "ord_from"                  => $_POST["ord_from"],
                    "ord_commentcust"           => $_POST["ord_commentcust"],
                    "ord_createdate"            => $_POST["ord_createdate"],
                    "ord_createdip"             => $_POST["ord_createdip"],
                    "ord_cancelreason"          => ""
                );

                //Save order main record tbl_orders
                $this->load->model("Orders_model");
                $newOrder = $this->Orders_model->saveOrder($dataOrder);

                $discountTotal = $_POST["ord_discounttotal"] * 1;
                if ($discountTotal > 0) {
                    $this->storeDiscount($_POST["ord_from"], $_POST["ord_customerid"], $_POST["ord_createdate"], $newOrder, $isFirstTime);
                }

                //Save order details
                $products = json_decode($_POST["products"]);
                $this->load->model("Orderitems_model");
                $this->load->model("Ordertopping_model");
                foreach ($products as $key => $value) {
                    $dataDetails= array(
                        "ori_restid"            => 6,
                        "ori_orderid"           => $newOrder,
                        "ori_prodid"            => $value->ori_prodid,
                        "ori_price"             => $value->individual_price,
                        "ori_usedpricelevel"    => $value->ori_usdpricelevel,
                        "ori_quantity"          => $value->ori_quantity,
                        "ori_basetype"          => $value->ori_basetype,
                        "ori_halfandhalf"       => $value->ori_halfandhalf,
                        "ori_halfandhalfwith"   => ($value->ori_halfandhalfwith > 0 ? $value->ori_halfandhalfwith : NULL),
                        "ori_sizeselection"     => (strcmp($value->ori_sizeselection, "Small") == 0 ? $value->ori_sizeselection : '')
                    );
                    $itemOrdered = $this->Orderitems_model->saveOrder($dataDetails);

                    $extraTopping = (isset($value->extras) ? $value->extras : $value->topping);
                    foreach ($extraTopping as $keyx => $valuex) {
                        $topping = array(
                            "oro_restid"            => 6,
                            "oro_orderdetailid"     => $itemOrdered,
                            "oro_productid"     => $valuex->oro_productid,
                            "oro_optype"        => $valuex->oro_optype,
                            "oro_name"          => $valuex->oro_name,
                            "oro_price"         => (strcmp($valuex->oro_optype, "R") == 0 ? 0 : $valuex->oro_price)
                        );
                        $this->Ordertopping_model->saveOrder($topping);

                        if (strcmp($valuex->oro_optype, "O") == 0) {
                            $dataType = array(
                                "ori_selectedoption"    => $valuex->oro_name
                            );
                            $this->Orderitems_model->updateType($itemOrdered, $dataType);
                        }
                    }
                }

                $backMessage = ($_POST["ord_deliverytype"] == 1 ? "DELIVERY" : "PICKUP");
                if (strcmp($backMessage, "DELIVERY") == 0) {
                    $devTime = '';
                }
                $bodyMessage = "Your ".$backMessage." Order has been successfully Processed. \n \n Very soon, You will receive an SMS from us regarding, your ".$backMessage." Time. A copy of your tax invoice will also be sent to your email address. \n \n PLS. Contact us if you DO NOT receive a confirmation SMS from us within 15 minutes.";

                $this->load->model("Paymentprovider_model");
                $provider = $this->Paymentprovider_model->getRecord($_POST["ord_paymenttype"]);
                $orderDetails = $this->Orders_model->getOrderDetailsByID($newOrder);

                switch ($provider->pay_id) {
                    case 1:
                        $detailPaymentTool = $orderDetails->ord_creditcardno;
                        break;
                    case 2:
                        $detailPaymentTool = $customerEmail;
                        break;
                    default:
                        $detailPaymentTool = "";
                }

                $this->load->model("Customers_model");
                $customer_id = (isset($_POST["customer_id"]) ? $_POST["customer_id"] : $_POST["ord_customerid"]);
                $customer = $this->Customers_model->getRecordById($customer_id);
                $custAddress = $customer->adr_unitnumber.'/'.$customer->adr_streetnumber.' '.$customer->sbrc_streetname.', '.$customer->sbrb_name;

                //SEND EMAIL
                $emailBody = $this->createEmailConfirmation($_POST["ord_number"], $customer->cust_name.' '.$customer->cust_surname, $customer->cust_mobphone, $custAddress, $products, $_POST["ord_deliveryfee"], $_POST["ord_paymentfee"], $_POST["ord_discounttotal"], $_POST["ord_createdate"], $devTime, $provider->pay_shortname, $_POST["ord_deliveryoption"], $_POST["ord_deliverytype"], $detailPaymentTool, $_POST["ord_subtotal"], $totalAmount, $_POST["ord_commentcust"]);

                require 'assets/includes/email/PHPMailerAutoload.php';
                $this->sendEmail($customerEmail, 'Order Confirmation: '.$_POST["ord_number"], $emailBody);

//                $smsMessage = "Your ".$backMessage." Order has been successfully Processed. Very soon, You will receive an
//                SMS from us regarding, your Pickup Time. A copy of your tax invoice will also be sent to your email address. PLS.
//                Contact us if you DO NOT receive a confirmation SMS from us within 15 minutes.";
//                $this->sendSMS($customer->cust_mobphone, $smsMessage);

                $response = array("success" => true, "content" => $_POST, "xml" => $paymentResponse["responseData"], "data" => $dataOrder ,"message" => $bodyMessage);
            } else {
                $response = array("success" => false, "message" => $paymentResponse["responseData"]["responseMessage"]);
            }

        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }
        $response = json_encode($response);
        print $response;
    }

    function getOrderNumber() {
        if (isset($_GET['key']) && $_GET['key'] == $this->getKey()) {
            $randomString = $this->getOrderCodeNumber();
            $response = array("success" => true, "number" => $randomString);
        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }
        $response = json_encode($response, true);
        print $response;
    }

    function getCountries() {
        if (isset($_GET['key']) && $_GET['key'] == $this->getKey()) {
            $this->load->model("Countries_model");
            $countries = $this->Countries_model->getAll();
            $response = array("success" => true, "content" => $countries);
        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }
        $response = json_encode($response, true);
        print $response;
    }
    //******

    //**** ADDRESS DATA
    function getSuburbs() {
        if (isset($_GET['key']) && $_GET['key'] == $this->getKey()) {
            $this->load->model("Suburbs_model");
            $this->load->model("Suburbstreets_model");
            $suburbs = $this->Suburbs_model->getAll();
            foreach ($suburbs as $key => $value) {
                $subStreet = $this->Suburbstreets_model->getBySuburb($value->sbrb_id);
                $value->Suburbstreets = $subStreet;
            }
            $response = array("success" => true, "content" => $suburbs);
        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }
        $response = json_encode($response, true);
        print $response;
    }

    //*******

    /**
     * Return an array with all shcedule available for reservation in a week
     * Starting from Sunday to Saturday
     * @params
     * - Key
     */
    function getAllSchedule() {
        if (isset($_GET['key']) && $_GET['key'] == $this->getKey()) {
            $this->load->model("Schedule_model");
            $schedule = $this->Schedule_model->getScheduleByDay();

            $responseArray  = array();
            $sundayTime     = array();
            $mondayTime     = array();
            $tuesdayTime    = array();
            $wednesdayTime  = array();
            $thursdayTime   = array();
            $fridayTime     = array();
            $saturdayTime   = array();

            //SUNDAY Array
            if (strcmp($schedule->sunday_open_lunch, $schedule->sunday_close_lunch) != 0) {
                $initTime = date("H:i", strtotime($schedule->sunday_open_lunch));
                $endTime = date("H:i", strtotime($schedule->sunday_close_lunch));
                for ($i = 0; $initTime < $endTime; $i++) {
                    array_push($sundayTime, $initTime);
                    $timeTime = strtotime($initTime);
                    $initTime = date("H:i", strtotime("+15 minutes", $timeTime));
                }
            }

            if (strcmp($schedule->sunday_open, $schedule->sunday_close) != 0) {
                $initTime = date("H:i", strtotime($schedule->sunday_open));
                $endTime = date("H:i", strtotime($schedule->sunday_close));
                for ($i = 0; $initTime < $endTime; $i++) {
                    array_push($sundayTime, $initTime);
                    $timeTime = strtotime($initTime);
                    $initTime = date("H:i", strtotime("+15 minutes", $timeTime));
                }
            }
            array_push($responseArray, $sundayTime);

            //MONDAY Array
            if (strcmp($schedule->monday_open_lunch, $schedule->monday_close_lunch) != 0) {
                $initTime = date("H:i", strtotime($schedule->monday_open_lunch));
                $endTime = date("H:i", strtotime($schedule->monday_close_lunch));
                for ($i = 0; $initTime < $endTime; $i++) {
                    array_push($mondayTime, $initTime);
                    $timeTime = strtotime($initTime);
                    $initTime = date("H:i", strtotime("+15 minutes", $timeTime));
                }
            }

            if (strcmp($schedule->monday_open, $schedule->monday_close) != 0) {
                $initTime = date("H:i", strtotime($schedule->monday_open));
                $endTime = date("H:i", strtotime($schedule->monday_close));
                for ($i = 0; $initTime < $endTime; $i++) {
                    array_push($mondayTime, $initTime);
                    $timeTime = strtotime($initTime);
                    $initTime = date("H:i", strtotime("+15 minutes", $timeTime));
                }
            }
            array_push($responseArray, $mondayTime);

            //TUESDAY Array
            if (strcmp($schedule->tuesday_open_lunch, $schedule->tuesday_close_lunch) != 0) {
                $initTime = date("H:i", strtotime($schedule->tuesday_open_lunch));
                $endTime = date("H:i", strtotime($schedule->tuesday_close_lunch));
                for ($i = 0; $initTime < $endTime; $i++) {
                    array_push($tuesdayTime, $initTime);
                    $timeTime = strtotime($initTime);
                    $initTime = date("H:i", strtotime("+15 minutes", $timeTime));
                }
            }

            if (strcmp($schedule->tuesday_open, $schedule->tuesday_close) != 0) {
                $initTime = date("H:i", strtotime($schedule->tuesday_open));
                $endTime = date("H:i", strtotime($schedule->tuesday_close));
                for ($i = 0; $initTime < $endTime; $i++) {
                    array_push($tuesdayTime, $initTime);
                    $timeTime = strtotime($initTime);
                    $initTime = date("H:i", strtotime("+15 minutes", $timeTime));
                }
            }
            array_push($responseArray, $tuesdayTime);

            //WEDNESDAY Array
            if (strcmp($schedule->wednesday_open_lunch, $schedule->wednesday_close_lunch) != 0) {
                $initTime = date("H:i", strtotime($schedule->wednesday_open_lunch));
                $endTime = date("H:i", strtotime($schedule->wednesday_close_lunch));
                for ($i = 0; $initTime < $endTime; $i++) {
                    array_push($wednesdayTime, $initTime);
                    $timeTime = strtotime($initTime);
                    $initTime = date("H:i", strtotime("+15 minutes", $timeTime));
                }
            }

            if (strcmp($schedule->wednesday_open, $schedule->wednesday_close) != 0) {
                $initTime = date("H:i", strtotime($schedule->wednesday_open));
                $endTime = date("H:i", strtotime($schedule->wednesday_close));
                for ($i = 0; $initTime < $endTime; $i++) {
                    array_push($wednesdayTime, $initTime);
                    $timeTime = strtotime($initTime);
                    $initTime = date("H:i", strtotime("+15 minutes", $timeTime));
                }
            }
            array_push($responseArray, $wednesdayTime);

            //THURSDAY Array
            if (strcmp($schedule->thursday_open_lunch, $schedule->thursday_close_lunch) != 0) {
                $initTime = date("H:i", strtotime($schedule->thursday_open_lunch));
                $endTime = date("H:i", strtotime($schedule->thursday_close_lunch));
                for ($i = 0; $initTime < $endTime; $i++) {
                    array_push($thursdayTime, $initTime);
                    $timeTime = strtotime($initTime);
                    $initTime = date("H:i", strtotime("+15 minutes", $timeTime));
                }
            }

            if (strcmp($schedule->thursday_open, $schedule->thursday_close) != 0) {
                $initTime = date("H:i", strtotime($schedule->thursday_open));
                $endTime = date("H:i", strtotime($schedule->thursday_close));
                for ($i = 0; $initTime < $endTime; $i++) {
                    array_push($thursdayTime, $initTime);
                    $timeTime = strtotime($initTime);
                    $initTime = date("H:i", strtotime("+15 minutes", $timeTime));
                }
            }
            array_push($responseArray, $thursdayTime);

            //FRIDAY Array
            if (strcmp($schedule->friday_open_lunch, $schedule->friday_close_lunch) != 0) {
                $initTime = date("H:i", strtotime($schedule->friday_open_lunch));
                $endTime = date("H:i", strtotime($schedule->friday_close_lunch));
                for ($i = 0; $initTime < $endTime; $i++) {
                    array_push($fridayTime, $initTime);
                    $timeTime = strtotime($initTime);
                    $initTime = date("H:i", strtotime("+15 minutes", $timeTime));
                }
            }

            if (strcmp($schedule->friday_open, $schedule->friday_close) != 0) {
                $initTime = date("H:i", strtotime($schedule->friday_open));
                $endTime = date("H:i", strtotime($schedule->friday_close));
                for ($i = 0; $initTime < $endTime; $i++) {
                    array_push($fridayTime, $initTime);
                    $timeTime = strtotime($initTime);
                    $initTime = date("H:i", strtotime("+15 minutes", $timeTime));
                }
            }
            array_push($responseArray, $fridayTime);

            //SATURDAY Array
            if (strcmp($schedule->saturday_open_lunch, $schedule->saturday_close_lunch) != 0) {
                $initTime = date("H:i", strtotime($schedule->saturday_open_lunch));
                $endTime = date("H:i", strtotime($schedule->saturday_close_lunch));
                for ($i = 0; $initTime < $endTime; $i++) {
                    array_push($saturdayTime, $initTime);
                    $timeTime = strtotime($initTime);
                    $initTime = date("H:i", strtotime("+15 minutes", $timeTime));
                }
            }

            if (strcmp($schedule->saturday_open, $schedule->saturday_close) != 0) {
                $initTime = date("H:i", strtotime($schedule->saturday_open));
                $endTime = date("H:i", strtotime($schedule->saturday_close));
                for ($i = 0; $initTime < $endTime; $i++) {
                    array_push($saturdayTime, $initTime);
                    $timeTime = strtotime($initTime);
                    $initTime = date("H:i", strtotime("+15 minutes", $timeTime));
                }
            }
            array_push($responseArray, $saturdayTime);


            $response = array("success" => true, "content" => $responseArray);
        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }
        $response = json_encode($response, true);
        print $response;
    }

    function checkIfOpen() {
        if (isset($_GET['key']) && $_GET['key'] == $this->getKey()) {
            $this->load->model("Schedule_model");

            $timezone = 'Australia/Sydney';  //perl: $timeZoneName = "MY TIME ZONE HERE";
            $date = new DateTime('now', new DateTimeZone($timezone));
            $today = $date->format('Y-m-d H:i:s');
            $checkTime =  explode(" ", $today);
            $weekDay = date("N", strtotime($today));
            switch ($weekDay) {
                case 1:
                    $check = $this->Schedule_model->checkIfOpen($checkTime[1], "monday_open_lunch", "monday_close_lunch");
                    if (empty($check)) {
                        $check = $this->Schedule_model->checkIfOpen($checkTime[1], "monday_open", "monday_close");
                    }
                    break;
                case 2:
                    $check =  $this->Schedule_model->checkIfOpen($checkTime[1], "tuesday_open_lunch", "tuesday_close_lunch");
                    if (empty($check)) {
                        $check = $this->Schedule_model->checkIfOpen($checkTime[1], "tuesday_open", "tuesday_close");
                    }
                    break;
                case 3:
                    $check =  $this->Schedule_model->checkIfOpen($checkTime[1], "wednesday_open_lunch", "wednesday_close_lunch");
                    if (empty($check)) {
                        $check = $this->Schedule_model->checkIfOpen($checkTime[1], "wednesday_open", "wednesday_close");
                    }
                    break;
                case 4:
                    $check =  $this->Schedule_model->checkIfOpen($checkTime[1], "thursday_open_lunch", "thursday_close_lunch");
                    if (empty($check)) {
                        $check = $this->Schedule_model->checkIfOpen($checkTime[1], "thursday_open", "thursday_close");
                    }
                    break;
                case 5:
                    $check =  $this->Schedule_model->checkIfOpen($checkTime[1], "friday_open_lunch", "friday_close_lunch");
                    if (empty($check)) {
                        $check = $this->Schedule_model->checkIfOpen($checkTime[1], "friday_open", "friday_close");
                    }
                    break;
                case 6:
                    $check =  $this->Schedule_model->checkIfOpen($checkTime[1], "saturday_open_lunch", "saturday_close_lunch");
                    if (empty($check)) {
                        $check = $this->Schedule_model->checkIfOpen($checkTime[1], "saturday_open", "saturday_close");
                    }
                    break;
                case 7:
                    $check =  $this->Schedule_model->checkIfOpen($checkTime[1], "sunday_open_lunch", "sunday_close_lunch");
                    if (empty($check)) {
                        $check = $this->Schedule_model->checkIfOpen($checkTime[1], "sunday_open", "sunday_close");
                    }
                    break;
                default:
                    $check = array();
            }

            if (!empty($check)) {
                $response = array("success" => true, "message" => "The Restaurant is open");
            } else {
                $response = array("success" => false, "message" => "Currently Closed");
            }
        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }
        $response = json_encode($response, true);
        print $response;
    }

    function checkIfOpenByTime() {
        if (isset($_GET['key']) && $_GET["time"] && $_GET["date"] && $_GET["type_delivery"] && $_GET['key'] == $this->getKey()) {
            $this->load->model("Schedule_model");
            $weekDay = date("N", strtotime($_GET["date"]));

            $checkTime = $_GET["time"];

            switch ($weekDay) {
                case 1:
                    if ($_GET["type_delivery"] == 2) {
                        $check = $this->Schedule_model->checkIfOpen($checkTime, "monday_open_lunch", "monday_close_lunch");
                        if (empty($check)) {
                            $check = $this->Schedule_model->checkIfOpen($checkTime, "monday_open", "monday_close");
                        }
                    } else {
                        $check = $this->Schedule_model->checkIfOpen($checkTime, "monday_open", "monday_close");
                    }
                    break;
                case 2:
                    if ($_GET["type_delivery"] == 2) {
                        $check = $this->Schedule_model->checkIfOpen($checkTime, "tuesday_open_lunch", "tuesday_close_lunch");
                        if (empty($check)) {
                            $check = $this->Schedule_model->checkIfOpen($checkTime, "tuesday_open", "tuesday_close");
                        }
                    } else {
                        $check = $this->Schedule_model->checkIfOpen($checkTime, "tuesday_open", "tuesday_close");
                    }
                    break;
                case 3:
                    if ($_GET["type_delivery"] == 2) {
                        $check = $this->Schedule_model->checkIfOpen($checkTime, "wednesday_open_lunch", "wednesday_close_lunch");
                        if (empty($check)) {
                            $check = $this->Schedule_model->checkIfOpen($checkTime, "wednesday_open", "wednesday_close");
                        }
                    } else {
                        $check = $this->Schedule_model->checkIfOpen($checkTime, "wednesday_open", "wednesday_close");
                    }
                    break;
                case 4:
                    if ($_GET["type_delivery"] == 2) {
                        $check = $this->Schedule_model->checkIfOpen($checkTime, "thursday_open_lunch", "thursday_close_lunch");
                        if (empty($check)) {
                            $check = $this->Schedule_model->checkIfOpen($checkTime, "thursday_open", "thursday_close");
                        }
                    } else {
                        $check = $this->Schedule_model->checkIfOpen($checkTime, "thursday_open", "thursday_close");
                    }
                    break;
                case 5:
                    if ($_GET["type_delivery"] == 2) {
                        $check = $this->Schedule_model->checkIfOpen($checkTime, "friday_open_lunch", "friday_close_lunch");
                        if (empty($check)) {
                            $check = $this->Schedule_model->checkIfOpen($checkTime, "friday_open", "friday_close");
                        }
                    } else {
                        $check = $this->Schedule_model->checkIfOpen($checkTime, "friday_open", "friday_close");
                    }
                    break;
                case 6:
                    if ($_GET["type_delivery"] == 2) {
                        $check = $this->Schedule_model->checkIfOpen($checkTime, "saturday_open_lunch", "saturday_close_lunch");
                        if (empty($check)) {
                            $check = $this->Schedule_model->checkIfOpen($checkTime, "saturday_open", "saturday_close");
                        }
                    } else {
                        $check = $this->Schedule_model->checkIfOpen($checkTime, "saturday_open", "saturday_close");
                    }
                    break;
                case 7:
                    if ($_GET["type_delivery"] == 2) {
                        $check = $this->Schedule_model->checkIfOpen($checkTime, "sunday_open_lunch", "sunday_close_lunch");
                        if (empty($check)) {
                            $check = $this->Schedule_model->checkIfOpen($checkTime, "sunday_open", "sunday_close");
                        }
                    } else {
                        $check = $this->Schedule_model->checkIfOpen($checkTime, "sunday_open", "sunday_close");
                    }
                    break;
            }

            if (!empty($check)) {
                $response = array("success" => true, "message" => "The Restaurant is open");
            } else {
                $response = array("success" => false, "message" => "The Restaurant is closed at the Date and Time you selected. Remember, the delivery service starts at 6:00 pm");
            }
        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }
        $response = json_encode($response, true);
        print $response;
    }

    // MARK : PROFILE
    function getOrdersByCustomer() {
        if (isset($_GET["key"]) && $_GET["key"] == $this->getKey() && isset($_GET["customers_id"])) {
            $this->load->model("Customers_model");
            $customer = $this->Customers_model->getRecordById($_GET["customers_id"]);
            if (!empty($customer)) {
                $this->load->model("Orders_model");
                $orders =  $this->Orders_model->getOrdersByCustomer($_GET["customers_id"]);
                if (!empty($orders)) {
                    $this->load->model("Orderitems_model");
                    $this->load->model("Ordertopping_model");
                    $this->load->model("Product_model");
                    foreach ($orders as $key => $value) {
                        $itemsOrdered = $this->Orderitems_model->getItemsByOrder($value->ord_id);
                        $value->details = $itemsOrdered;
                        $value->ord_createdate = date("M d, Y - h:i a", strtotime($value->ord_createdate));
                        foreach ($itemsOrdered as $item => $val) {
                            $productDetail = $this->Product_model->getRecordByIdByGroup($val->ori_prodid, 1);
                            if (!empty($productDetail)) {
                                $val->ori_price = $this->checkPriceBySize($val, $productDetail);
                                $val->available = true;
                            } else {
                                $val->available = false;
                            }

                            if ($val->prd_showonweb == 0) {
                                $val->available = false;
                            }

                            if ($val->ori_halfandhalfwith > 0) {
                                $val->prd_namehalf = $this->Product_model->getRecord($val->ori_halfandhalfwith)->prd_name;
                                $val->ori_price = $this->Product_model->getPriceProductSize($val->ori_halfandhalfwith, "large")->price;
                            }
                            $topping = $this->Ordertopping_model->getItemsByDetail($val->ori_id);
                            $val->topping = $topping;
                        }
                    }
                    $response = array("success" => true, "content" => $orders);
                } else {
                    $response = array("success" => false, "message" => "No orders placed from Little Piazza yet");
                }
            } else {
                $response = array("success" => false, "message" => "Your user is invalid, please SignOut and SignIn again");
            }
        } else {
            $response = array("success" => false, "message" => "Wrong parameters received");
        }
        $response = json_encode($response, true);
        print($response);
    }
    //*****

    //*** UTILS


    private function getDeliveryOption($operativeSystem, $deliveryType, $deliveryTime) {
        if (strcmp($operativeSystem, "And_App") == 0) {
            return $deliveryType - 1;
        }
        return $deliveryType - 1;
    }

    /**
     * This function check the size from a product in a past order and verify if the actual price is updated
     * @param $productBought
     * @param $productActive
     * @return Right price for the product
     */
    private function checkPriceBySize($productBought, $productActive) {
        switch ($productBought->ori_sizeselection) {
            case "Small":
                return $productActive->prd_smallprice;
                break;
            case "Large":
                return $productActive->prd_largeprice;
                break;
            default:
                return $productActive->prd_fixedprice;
        }
    }

    private function checkSizesAvailable($small, $medium, $large) {
        $ans = array();
        if ($small > 0) {
            $ans[]   = array("pre_type" => "I", "id" => "1", "name" => "Small", "price" => $small, "selected" => false);
        }

        if ($medium > 0) {
            $ans[]   = array("pre_type" => "I", "id" => "2", "name" => "Medium", "price" => $medium, "selected" => false);
        }

        if ($large > 0) {
            $ans[]   = array("pre_type" => "I", "id" => "3", "name" => "Large", "price" => $large, "selected" => true);
        }
        return $ans;
    }

    private function getRandom() {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 8; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    private function getOrderCodeNumber() {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 5; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        $this->load->model("Orders_model");
        $retorno = $this->Orders_model->getOrderByNumber($randomString);
        if (!empty($retorno)) {
            $this->getOrderCodeNumber();
        } else {
            return $randomString;
        }
    }

    private function createEmailConfirmation($orderNumber, $clientName, $phoneNumber, $address, $products, $deliveryFee, $paymentFee, $discount, $devTime, $toDelivery, $provider, $orderfor, $deliveryType, $paymentTool, $subTotal, $total, $comments) {


        $arrayContent = $this->loadProductsEmail($products);
        $tableResponse = $arrayContent["content"];
        $tableContent = '';
        foreach ($tableResponse as $keyTable => $tableValue) {
            $tableContent .= $tableValue["product"];
            $tableContent .= $tableValue["extras"];
        }

        //add sub total to the table
        $tableContent .= '<tr style="background-color:#E6E6E6;">
                    <td width="10%">&nbsp;</td>
                    <td>Sub Total</td>
                    <td align="right" width="20%">$'.number_format($subTotal, 2).'</td>
                  </tr>';

        $deliveryContent = "";
        if ($deliveryFee > 0) {
            $deliveryContent = '
                <tr style="background-color: #E6E6E6;">
                    <td width="10%">&nbsp;</td>
                    <td>Delivery</td>
                    <td align="right" width="20%">$'.number_format($deliveryFee, 2).'</td>
                </tr>
            ';
            $tableContent .= $deliveryContent;
        }

        $payFeeContent = "";
        if ($paymentFee > 0) {
            $payFeeContent = '<tr style="background-color: #E6E6E6;">
                    <td width="10%">&nbsp;</td>
                    <td>Card Fee</td>
                    <td align="right" width="20%">$'.number_format($paymentFee, 2).'</td>
                </tr>
            ';
            $tableContent .= $payFeeContent;
        }

        $discountContent = "";
        if ($discount > 0) {
            $discountContent = '
                <tr style="background-color: #E6E6E6;">
                    <td width="10%">&nbsp;</td>
                    <td>Discount</td>
                    <td align="right" width="20%">-$'.number_format($discount, 2).'</td>
                </tr>
            ';
            $tableContent .= $discountContent;
        }

        $tableContent .= '<tr style="background-color: #E6E6E6;">
                    <td width="10%">&nbsp;</td>
                    <td><strong>Total</strong></td>
                    <td align="right" width="20%">$'.number_format($total, 2).'</td>
                  </tr>';

        $totalAmount = $subTotal + $deliveryFee + $paymentFee - $discount;

        $deliveryTime = explode(" ", $devTime);

        $orderwhen = "";
        if ($orderfor == 1) {
            switch ($deliveryType) {
                case 1:
                    $devOrderTitle = "DELIVERY";
                    break;
                case 2:
                    $devOrderTitle = "PICKUP";
                    break;
                default:
                    $devOrderTitle = "";
                    break;
            }
            $orderwhen = '<strong>
                    '.$devOrderTitle.' ORDER
                    <br>
                    Ordered At '.$deliveryTime[1].'
                    </strong>';
        }

        $devAddress = "";
        if ($deliveryType == 1) {
            $devAddress = '<p>
                    Deliver To:
                  <br>
                    '.$address.'
                    </p>
                <p>';
        }

        $orderComments = '';
        if (strlen($comments) > 0) {
            $orderComments = '<br>'.$comments.'<br>';
        }

        $devDescTime = (strlen($toDelivery) > 10 ? 'DEL. Time '.date("H:i:s A m/d/Y", strtotime($toDelivery)).'' : '');

        $body = '
            <html>
            <head>
              <title>Order Confirmation</title>
            </head>
            <body>
              <div align="center">
                <img src="http://littlepiazza.com.au/app/assets/img/logo.png" width="120px" height="120px">
                <p>
                    ABN 70 166 484 251 <br>
                  30 Gadigal Ave, Zetland PH: 9662 1227
                </p>
                <br>
                <p>
                  <strong>TAX INVOICE</strong>
                  <br>
                    Order ID &nbsp; &nbsp; '.$orderNumber.'
                    </p>
                    <br>
                <p>
                    '.$clientName.'
                <br>
                  '.$phoneNumber.'
                </p>

                '.$devAddress.'

                '.$orderComments.'

                <br>
                  <strong>YOU ORDERED</strong>
                </p>
              </div>
              <div align="center">
                <table>
                  '.$tableContent.'
                </table>
              </div>
              <div align="center">
                <p><strogn>PMT &nbsp; &nbsp; '.$provider.'</strong></p>
                <br>
                <span style="font-size: 8px;">'.$paymentTool.'</span>
                <br>
                <br>
                <p>
                  '.$orderwhen.'
                  <br>
                    <span style="text-size:8px">'.$devDescTime.'</span>
                </p>
              </div>
            </body>
            </html>
        ';
        return $body;
    }

    private function loadProductsEmail($products) {

        $subTotal = 0;
        $arrayResponse = [];
        foreach ($products as $key => $value) {

            $subTotal += $value->ori_price;

            $size = (strcmp($value->ori_sizeselection, "Small") == 0 ? $value->ori_sizeselection : '');

            $tableContent = '
                <tr>
                    <td width="10%">'.$value->ori_quantity.'</td>
                    <td>
                        '.($value->ori_halfandhalf  == 1 ? $value->prd_name." / ".$value->prd_namehalf : $value->prd_name).' '.$size.'
                    </td>
                    <td width="20%" style="text-align:right;">$'.(number_format($value->individual_price,2) * $value->ori_quantity).'</td>
                </tr>
            ';

            $productContent = '';
            $addContent     = '';
            $removeContent  = '';
            $baseContent    = '';
            $sauceContent   = '';
            $optionContent  = '';
            $extras = (isset($value->extras) ? $value->extras : $value->topping);
            foreach ($extras as $keyx => $valuex) {
                $priceItem = number_format($valuex->oro_price,2) * $value->ori_quantity;
                switch ($valuex->oro_optype) {
                    case "A":
                        $valuex->oro_optype = "ADD - ";
                        $addContent .= '<tr>
                            <td width="10%">&nbsp;</td>
                            <td>
                                '.$valuex->oro_optype.$valuex->oro_name.'
                            </td>
                            <td width="20%" style="text-align:right;">'.($valuex->oro_price > 0 && strcmp($valuex->oro_optype, "R") != 0 ? '$'.number_format($priceItem,2) : "").'</td>
                        </tr>';
                        break;
                    case "R":
                        $valuex->oro_optype = "MINUS - ";
                        $removeContent .= '<tr>
                            <td width="10%">&nbsp;</td>
                            <td>
                                '.$valuex->oro_optype.$valuex->oro_name.'
                            </td>
                            <td width="20%" style="text-align:right;"></td>
                        </tr>';
                        break;
                    case "B":
                        $valuex->oro_optype = "BASE - ";
                        $baseContent .= '<tr>
                            <td width="10%">&nbsp;</td>
                            <td>
                                '.$valuex->oro_optype.$valuex->oro_name.'
                            </td>
                            <td width="20%" style="text-align:right;">'.($valuex->oro_price > 0 && strcmp($valuex->oro_optype, "R") != 0 ? '$'.number_format($priceItem,2) : "").'</td>
                        </tr>';
                        break;
                    case "S":
                        $valuex->oro_optype = "SAUCE - ";
                        $sauceContent .= '<tr>
                            <td width="10%">&nbsp;</td>
                            <td>
                                '.$valuex->oro_optype.$valuex->oro_name.'
                            </td>
                            <td width="20%" style="text-align:right;">'.($valuex->oro_price > 0 && strcmp($valuex->oro_optype, "R") != 0 ? '$'.number_format($priceItem,2) : "").'</td>
                        </tr>';
                        break;
                    default:
                        $valuex->oro_optype = "";
                        $optionContent .= '<tr>
                            <td width="10%">&nbsp;</td>
                            <td>
                                '.$valuex->oro_optype.$valuex->oro_name.'
                            </td>
                            <td width="20%" style="text-align:right;">'.($valuex->oro_price > 0 && strcmp($valuex->oro_optype, "R") != 0 ? '$'.number_format($priceItem,2) : "").'</td>
                        </tr>';
                }

//                $productContent .= '<tr>
//                    <td width="10%">&nbsp;</td>
//                    <td>
//                        '.$valuex->oro_optype.$valuex->oro_name.'
//                    </td>
//                    <td width="20%" style="text-align:right;">'.($valuex->oro_price > 0 && strcmp($valuex->oro_optype, "R") != 0 ? '$'.number_format($valuex->oro_price,2) : "").'</td>
//                </tr>';
                if ($valuex->oro_price > 0 && strcmp($valuex->oro_optype, "R") != 0) {
                    $subTotal += $valuex->oro_price;
                }
            }

            $productContent .= $optionContent;
            $productContent .= $baseContent;
            $productContent .= $sauceContent;
            $productContent .= $addContent;
            $productContent .= $removeContent;

            $arrayResponse[] = array("product" => $tableContent, "extras" => $productContent);

        }

        return array("content" => $arrayResponse, "subtotal" => $subTotal);
    }

    private function sendSMS($phone, $message) {
        $url = 'https://www.5centsms.com.au/api/v4/sms';

        $fields = array(
            'sender' => urlencode('LilPiazza'),
            'to' => urlencode($phone),
            'message' => urlencode($message),
            'test' => urlencode('false'),

        );
        $fields_string = "";
        foreach($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'User: info@littlepiazza.com.au',
            'Api-Key: 9ZxFH2XsLYgXK4ofEa82k52LM49cS9',
//            'Api-Key: NdptbFCJVF9gSYBk9vgcsHnMRJZvtC',
        ));

        $result = curl_exec($ch);
        curl_close($ch);

//        $response = json_decode($result, true);
//        var_dump($result);
//        print_r($response);
//        var_dump($response);

    }

    function sendErrorEmail() {
        require 'assets/includes/email/PHPMailerAutoload.php';
        $this->sendEmail($_POST["email"], "Error found in Android App", $_POST["message"]);
    }

    private function getKey() {
        return '05130d60b3bee9339c7b4fb9e6fc83b6';
    }

    private function getEmptyRecordsMessage() {
         return "No results found in your request";
    }

    private function incompleteParametersMessage() {
        return array("success" => false, "message" => "Wrong Parameters");
    }

    private function sendEmail($customerEmail, $subject, $body) {

        $host       = "littlepiazza.com.au";
        $username   = 'test@littlepiazza.com.au';
        $password   = 'Xni5b2?7';
//        $host       = "mail.mipsicomama.com";
//        $username   = 'info@mipsicomama.com';
//        $password   = 'silver13freak10';

//        require 'assets/includes/email/PHPMailerAutoload.php';

        $mail = new PHPMailer();

        $mail->isSMTP();                                // Set mailer to use SMTP
        $mail->Host = $host;                            // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                         // Enable SMTP authentication
        $mail->Username = $username;                    // SMTP username
        $mail->Password = $password;                    // SMTP password
        $mail->SMTPSecure = '';                         // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 25;                               // TCP port to connect to

        $mail->setFrom('noreply@littlepiazza.com.au', 'Little Piazza');
        $mail->addAddress($customerEmail);        // Add a recipient
        $mail->addReplyTo('noreply@littlepiazza.com.au', 'Little Piazza');
        $mail->addBCC('info@littlepiazza.com.au');

        $mail->isHTML(true);                                    // Set email format to HTML

        $mail->Subject = $subject;
        $mail->Body    = $body;

        if(!$mail->send()) {
//            echo 'Message could not be sent.';
//            echo 'Mailer Error: ' . $mail->ErrorInfo;
            return false;
        } else {
            return true;
//            echo 'Message has been sent';
        }
    }

    private function storeDiscount($orderedFrom, $customerId, $dateRegistered, $orderId, $isFirstTime) {
        $this->load->model("Device_model");
        if (strcmp($orderedFrom, "And_App") == 0) {
            $deviceOs = "android";
        } else {
            $deviceOs = "ios";
        }
        $deviceCustomer = $this->Device_model->getDeviceByUserOs($customerId, $deviceOs);
        if (!empty($deviceCustomer) || $isFirstTime == 1 || $isFirstTime === true) {
            $this->load->model("Orderdevice_model");
            $orderDevice = array(
                "token"         => (isset($deviceCustomer->token) ? $deviceCustomer->token : 'UserTurnedOffNotifications'),
                "customer_id"   => $customerId,
                "order_id"      => $orderId,
                "isFirstTime"   => $isFirstTime,
                "created_at"    => $dateRegistered,
                "updated_at"    => $dateRegistered
            );
            $this->Orderdevice_model->create($orderDevice);
        }

        $this->load->model("Customerdiscount_model");
        $custDiscount = array(
            "customer_id"   => $customerId,
            "discount_id"   => 0,
            "date_created"  => $dateRegistered,
            "date_updated"  => $dateRegistered
        );
        $this->Customerdiscount_model->create($custDiscount);
    }

    function sendEmail2() {
        $customerEmail = "villajohn@yahoo.com";
        $orderNumber = "ASJKD0AS";
        $body = "<strong>Pepe trueno</strong>";
        $host       = "littlepiazza.com.au";
        $username   = 'test@littlepiazza.com.au';
        $password   = 'Xni5b2?7';

        require 'assets/includes/email/PHPMailerAutoload.php';


        $mail = new PHPMailer;

////$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                // Set mailer to use SMTP
        $mail->Host = $host;                            // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                         // Enable SMTP authentication
        $mail->Username = $username;                    // SMTP username
        $mail->Password = $password;                    // SMTP password
        $mail->SMTPSecure = '';                         // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 25;                               // TCP port to connect to

        $mail->setFrom('noreply@littlepiazza.com.au', 'Little Piazza');
        $mail->addAddress($customerEmail);        // Add a recipient
        $mail->addReplyTo('noreply@littlepiazza.com.au', 'Little Piazza');
//        $mail->addCC('cc@example.com');
//        $mail->addBCC('info@littlepiazza.com.au');
//
////        $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
////        $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        $mail->isHTML(true);                                    // Set email format to HTML
//
        $mail->Subject = 'Order Confirmation - '.$orderNumber.'';
        $mail->Body    = $body;
//        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        if(!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo 'Message has been sent';
        }
    }

    // MARK : TESTING
    function updateManagePassword() {
        $data = array("usr_password" => $_POST["password"]);
        $this->load->model("User_model");
        $update = $this->User_model->updatePassword($_POST["email"], $data);
        if ($update > 0) {
            $response = array("success" => true, "message" => "Cool, le cambiaste el password a: ".$_POST["email"]." en el perfil de Manager");
        } else {
            $response = array("success" => false, "message" => "No se pudo cambiar la clave");
        }
        $response = json_encode($response, true);
        print $response;
    }

    function updateDriverPassword() {
        $data = array("cust_password" => $_POST["password"]);
        $this->load->model("Drivers_model");
        $update = $this->Drivers_model->updatePassword($_POST["email"], $data);
        if ($update > 0) {
            $response = array("success" => true, "message" => "Cool, le cambiaste el password a: ".$_POST["email"]." en el perfil de Driver");
        } else {
            $response = array("success" => false, "message" => "No se pudo cambiar la clave");
        }
        $response = json_encode($response, true);
        print $response;
    }

    private function sendResponse($response) {
        $response = json_encode($response, true);
        print $response;
    }

    function testNotification() {
        $this->load->model('Device_model');
        $message = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Amet purus gravida quis blandit turpis. Eu augue ut lectus arcu bibendum at varius vel. Eu non diam phasellus vestibulum lorem sed risus. Nulla at volutpat diam ut venenatis tellus in metus vulputate. Dui vivamus arcu felis bibendum. Lacus vestibulum sed arcu non odio euismod lacinia at. Posuere urna nec tincidunt praesent. Leo in vitae turpis massa sed elementum. A lacus vestibulum sed arcu non odio euismod lacinia at. Viverra tellus in hac habitasse platea dictumst vestibulum rhoncus. Lorem ipsum dolor sit amet consectetur adipiscing elit duis. Luctus accumsan tortor posuere ac ut consequat.";
        $attachment = "https://cdn.computerhoy.com/sites/navi.axelspringer.es/public/styles/480/public/media/image/2018/01/283965-falcon-heavy-lanzamiento.jpg?itok=ugh0HOmv";
        $url = "https://f1.com";
        $title = "NexosPH Notification";
        $subTitle = "News";

        $token = "fqeftUpqEKg:APA91bGSTRL46mGEbeVCCPCpJLkCRdjv69scv_xVATYrNItyOIu9KlAxUGGXN6e30jpa9jFHKP9coU4K9MdjEcM1gurq0NIhjNrXF45GHDlKXEOVP9Q1fHnpzmeQrcTh3swiSOIm8bKS";

        $this->Device_model->sendAndroidPush($token, $title, $subTitle, $message, $attachment, $url, $notificationType = "regular");
    }

}

