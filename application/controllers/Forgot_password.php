<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Forgot_password extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {

        $data["page_title"] = "New Password";

        //*** FORM VALIDATIONS
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        $this->form_validation->set_rules('password1', 'Password', 'required');
        $this->form_validation->set_rules('password2', 'Password Confirmation', 'required|matches[password1]');
        //***

        if ($this->form_validation->run() == FALSE) {
            if (isset($_GET["token"]) && isset($_GET["customer"])) {
                $this->load->model("Forgot_model");
                $record = $this->Forgot_model->getRecordByTokenCustomer($_GET["token"], $_GET["customer"]);

                if (!empty($record)) {
                    $data["success"]  = 1;
                    $data["customer"] = $_GET["customer"];
                    $data["token"]    = $_GET["token"];
                } else {
                    $data["success"] = 0;
                    $data["message"] = "Your request is invalid";
                }

            } else {
                $data["success"] = 0;
                $data["message"] = "Wrong parameters";
            }

            $this->load->view('forgot', $data);
        } else {
            $this->load->model("Forgot_model");
            $record = $this->Forgot_model->getRecordByTokenCustomer($_POST["token"], $_POST["customers_id"]);
            if (!empty($record)) {
                $this->load->model("Customers_model");
                $this->Customers_model->updatePassword($_POST, $record->cust_id);
                $this->Forgot_model->updateRecord($record->id);
                unset($_POST);
                $data["success"] = 0;
                $data["message"] = "Your password has been updated";
            } else {
                unset($_POST);
                $data["success"] = 1;
            }
            $this->load->view('forgot', $data);
        }
    }

}