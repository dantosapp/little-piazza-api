<?php

class Productallows_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }


    /**
     * @param $product_id
     * @return mixed (Base types available for a product)
     */
    function getProductBaseTypes($product_id) {
        $this->db->select("tbl_productallows.pre_id, tbl_productallows.pre_type, tbl_productallows.pre_optionalid");
        $this->db->select("tbl_basetypes.bse_name, tbl_basetypes.bse_price");
        $this->db->where("tbl_productallows.pre_type", "B");
        $this->db->where("tbl_productallows.pre_prodid", $product_id);
        $this->db->from("tbl_productallows");
        $this->db->join("tbl_basetypes", "tbl_basetypes.bse_id = tbl_productallows.pre_optionalid");
        return $this->db->get()->result();
    }

    /**
     * @param $product_id
     * @param $option (Must be "A" for Adding options or "R" for removing options
     * @return mixed (Toppings Options for a Product)
     */
    function getProductToppings($product_id, $option) {
        $this->db->select("tbl_productallows.pre_id, tbl_productallows.pre_type, tbl_productallows.pre_optionalid");
        $this->db->select("tbl_toppings.top_name, tbl_toppings.top_price");
        $this->db->where("tbl_productallows.pre_type", $option);
        $this->db->where("tbl_productallows.pre_prodid", $product_id);
        $this->db->where("tbl_productallows.pre_showinweb", 1);
        $this->db->from("tbl_productallows");
        $this->db->join("tbl_toppings", "tbl_toppings.top_id = tbl_productallows.pre_optionalid");
        return $this->db->get()->result();
    }

    /**
     * @param $product_id
     * @return mixed (Sauces options for a product)
     */
    function getProductSauces($product_id) {
        $this->db->select("tbl_productallows.pre_id, tbl_productallows.pre_type, tbl_productallows.pre_optionalid");
        $this->db->select("tbl_sauces.suc_name, tbl_sauces.suc_price");
        $this->db->where("tbl_productallows.pre_type", "S");
        $this->db->where("tbl_sauces.suc_isdeleted", 0);
        $this->db->where("tbl_productallows.pre_prodid", $product_id);
        $this->db->from("tbl_productallows");
        $this->db->join("tbl_sauces", "tbl_sauces.suc_id = tbl_productallows.pre_optionalid");
        return $this->db->get()->result();
    }
}