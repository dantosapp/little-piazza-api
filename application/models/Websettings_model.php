<?php

class Websettings_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function getSettings($restaurant) {
        $this->db->select("*");
        $this->db->where("web_restid", $restaurant);
        $this->db->from("tbl_websitesettings");
        return $this->db->get()->row();
    }

}
