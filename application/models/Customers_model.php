<?php

class Customers_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function user_login($data) {
        $this->db->select("tbl_customers.cust_id, tbl_customers.cust_name, tbl_customers.cust_surname");
        $this->db->select("tbl_customers.cust_emailaddress, tbl_customers.cust_phone, tbl_customers.cust_mobphone");
        $this->db->select("tbl_customeraddress.adr_id, tbl_customeraddress.adr_suburb, tbl_customeraddress.adr_streetname");
        $this->db->select("tbl_customeraddress.adr_streetnumber, tbl_suburbs.sbrb_id, tbl_suburbs.sbrb_name");
        $this->db->select("tbl_customeraddress.adr_neareststreet, tbl_customeraddress.adr_unitnumber, tbl_customeraddress.adr_streetnumber");
        $this->db->select("tbl_suburbstreets.sbrc_id, tbl_suburbstreets.sbrc_streetname");
        $this->db->select("tbl_suburbs.sbrb_deliveryfee, tbl_suburbs.sbrb_deliveryfreelimit");
        $this->db->where('tbl_customers.cust_emailaddress', $data['cust_emailaddress']);
        $this->db->where('tbl_customers.cust_password', $data["cust_password"]);
        $this->db->where('cust_isdeleted', 0);
        $this->db->from('tbl_customers');
        $this->db->join("tbl_customeraddress", "tbl_customeraddress.adr_custid = tbl_customers.cust_id and tbl_customeraddress.adr_isactive = 1", "left");
        $this->db->join("tbl_suburbstreets", "tbl_suburbstreets.sbrc_id = tbl_customeraddress.adr_streetname", "left");
        $this->db->join("tbl_suburbs", "tbl_suburbs.sbrb_id = tbl_customeraddress.adr_suburb", "left");

        $result = $this->db->get()->row();
        unset($result->cust_password);
        return $result;
    }

    function getRecordByEmail($email) {
        $this->db->select("tbl_customers.cust_id, tbl_customers.cust_name, tbl_customers.cust_surname");
        $this->db->select("tbl_customers.cust_emailaddress, tbl_customers.cust_phone, tbl_customers.cust_mobphone");
        $this->db->select("tbl_customeraddress.adr_id, tbl_customeraddress.adr_suburb, tbl_customeraddress.adr_streetname");
        $this->db->select("tbl_customeraddress.adr_streetnumber, tbl_suburbs.sbrb_id, tbl_suburbs.sbrb_name");
        $this->db->select("tbl_customeraddress.adr_neareststreet, tbl_customeraddress.adr_unitnumber, tbl_customeraddress.adr_streetnumber");
        $this->db->select("tbl_suburbstreets.sbrc_id, tbl_suburbstreets.sbrc_streetname");
        $this->db->select("tbl_suburbs.sbrb_deliveryfee, tbl_suburbs.sbrb_deliveryfreelimit");
        $this->db->like("tbl_customers.cust_emailaddress", $email);
        $this->db->from("tbl_customers");
        $this->db->join("tbl_customeraddress", "tbl_customeraddress.adr_custid = tbl_customers.cust_id", "left");
        $this->db->join("tbl_suburbs", "tbl_suburbs.sbrb_id = tbl_customeraddress.adr_suburb", "left");
        $this->db->join("tbl_suburbstreets", "tbl_suburbstreets.sbrc_sbrbid = tbl_suburbs.sbrb_id", "left");
        return $this->db->get()->row();
    }

    function getRecordById($customer_id) {
        $this->db->select("tbl_customers.cust_id, tbl_customers.cust_name, tbl_customers.cust_surname");
        $this->db->select("tbl_customers.cust_emailaddress, tbl_customers.cust_phone, tbl_customers.cust_mobphone");
        $this->db->select("tbl_customeraddress.adr_id, tbl_customeraddress.adr_suburb, tbl_suburbstreets.sbrc_streetname");
        $this->db->select("tbl_customeraddress.adr_streetnumber, tbl_suburbs.sbrb_id, tbl_suburbs.sbrb_name");
        $this->db->select("tbl_customeraddress.adr_neareststreet, tbl_customeraddress.adr_unitnumber, tbl_customeraddress.adr_streetnumber");
        $this->db->select("tbl_suburbstreets.sbrc_id, tbl_suburbstreets.sbrc_streetname");
        $this->db->select("tbl_suburbs.sbrb_deliveryfee, tbl_suburbs.sbrb_deliveryfreelimit");
        $this->db->where("tbl_customers.cust_id", $customer_id);
        $this->db->from("tbl_customers");
        $this->db->join("tbl_customeraddress", "tbl_customeraddress.adr_custid = tbl_customers.cust_id", "left");
        $this->db->join("tbl_suburbs", "tbl_suburbs.sbrb_id = tbl_customeraddress.adr_suburb", "left");
        $this->db->join("tbl_suburbstreets", "tbl_customeraddress.adr_streetname = tbl_suburbstreets.sbrc_id", "left");
        return $this->db->get()->row();
    }

    function loginFacebook($data) {
        $this->db->select('cust_id AS id, cust_name AS name, cust_surname AS surname, cust_emailaddress AS email, cust_password AS password, cust_mobphone AS phone');
        $this->db->where('cust_emailaddress', $data['email']);
        $this->db->where('cust_isdeleted', 0);
        $this->db->from('tbl_customers');
        $result = $this->db->get()->row();

        if (!empty($result)) {
            $content = array(
                "id"        => $result->id,
                "email"     => $result->email,
                "name"      => $result->name,
                "surname"   => $result->surname,
                "phone"     => $result->phone );
            return array("success" => true, "content" => $content);
        } else {
            return array("success" => false, "message" => "User and password combination are incorrect");
        }
    }

    function registerFacebookUser($data) {
        $this->db->insert("tbl_customers", $data);
        return $this->db->insert_id();
    }

    function register($data, $address) {
        $this->db->trans_begin();
        $this->load->model("Customeraddress_model");
        if ($data["cust_id"] > 0) {
            $this->db->where("cust_id", $data["cust_id"]);
            $this->db->update("tbl_customers", $data);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $answer = array("success" => false, "customer" => $data["cust_id"]);
            } else {
                $address["adr_custid"] = $data["cust_id"];
                $addressUpdate = $this->Customeraddress_model->register($address);

                if ($addressUpdate["success"]) {
                    $this->db->trans_commit();
                    $details = $this->getDetails($data["cust_id"], $addressUpdate["customer"]);
                    $answer = array("success" => true, "customer" => $data["cust_id"], "content" => $details, "registro1" => "");
                } else {
                    $this->db->trans_rollback();
                    $answer = array("success" => false, "message" => "Can't finish the registering process, please try again");
                }
            }
        } else {
            $this->db->insert("tbl_customers", $data);
            $record = $this->db->insert_id();

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $answer = array("success" => false, "message" => "Can't finish the registering process, please try again");
            } else {
                if ($record > 0) {
                    $address["adr_custid"] = $record;
                    $address = $this->Customeraddress_model->register($address);

                    if ($address["success"]) {
                        $this->db->trans_commit();
                        $details = $this->getDetails($record, $address["customer"]);
                        $answer = array("success" => true, "customer" => $record, "content" => $details, "registro2" => "");
                    } else {
                        $this->db->trans_rollback();
                        $answer = array("success" => false, "message" => "Can't finish the registering process, please try again");
                    }

                } else {
                    $this->db->trans_rollback();
                    $answer = array("success" => false, "message" => "Can't finish the registering process, please try again");
                }
            }
        }
        return $answer;
    }

    function getDetails($customer) {
        $this->db->select("tbl_customers.cust_id, tbl_customers.cust_name, tbl_customers.cust_surname");
        $this->db->select("tbl_customers.cust_emailaddress, tbl_customers.cust_phone, tbl_customers.cust_mobphone");
        $this->db->select("tbl_customeraddress.adr_id, tbl_customeraddress.adr_suburb, tbl_customeraddress.adr_streetname");
        $this->db->select("tbl_customeraddress.adr_streetnumber, tbl_suburbs.sbrb_id, tbl_suburbs.sbrb_name");
        $this->db->select("tbl_customeraddress.adr_neareststreet, tbl_customeraddress.adr_unitnumber, tbl_customeraddress.adr_streetnumber");
        $this->db->select("tbl_suburbstreets.sbrc_id, tbl_suburbstreets.sbrc_streetname");
        $this->db->select("tbl_suburbs.sbrb_deliveryfee, tbl_suburbs.sbrb_deliveryfreelimit");
        $this->db->where("tbl_customers.cust_isdeleted", 0);
        $this->db->where("tbl_customers.cust_id", $customer);
        $this->db->from("tbl_customers");
        $this->db->join("tbl_customeraddress", "tbl_customeraddress.adr_custid = tbl_customers.cust_id");
        $this->db->join("tbl_suburbs", "tbl_suburbs.sbrb_id = tbl_customeraddress.adr_suburb");
        $this->db->join("tbl_suburbstreets", "tbl_suburbstreets.sbrc_id = tbl_customeraddress.adr_streetname");
        return $this->db->get()->row();
    }

    function updatePassword($data, $customer) {
        $dats = array("cust_password" => $data["password1"]);
        $this->db->where("cust_id", $customer);
        $this->db->update("tbl_customers", $dats);
    }

    //*** UTILITIES METHODS
    function checkIfEmailExists($email) {
        $this->db->select("cust_id, cust_allowloginmob, cust_password, cust_isdeleted");
        $this->db->like("cust_emailaddress", $email);
        $this->db->from("tbl_customers");
        return $this->db->get()->row();
    }

    function checkMobileNumberExists($number) {
        $this->db->select("cust_id, cust_allowloginmob, cust_emailaddress, cust_password, cust_isdeleted");
        $this->db->like("cust_mobphone", $number);
        $this->db->from("tbl_customers");
        return $this->db->get()->row();
    }

}
