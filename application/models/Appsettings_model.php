<?php

class Appsettings_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function getAll() {
        $this->db->select("tbl_appsettings.id, tbl_appsettings.policy, tbl_appsettings.first_time_active, tbl_appsettings.first_time_percentage");
        $this->db->select("tbl_appsettings.first_time_amount, tbl_appsettings.minimal_amount, tbl_appsettings.initial_message");
        $this->db->from("tbl_appsettings");
        return $this->db->get()->row();
    }

    function update($data) {
        $this->db->where("id", $data["id"]);
        $this->db->update("tbl_appsettings", $data);
    }

    function getActiveDiscount($amount) {
        $this->db->select("tbl_appsettings.id, tbl_appsettings.policy, tbl_appsettings.first_time_active, tbl_appsettings.first_time_percentage");
        $this->db->select("tbl_appsettings.first_time_amount, tbl_appsettings.minimal_amount, tbl_appsettings.initial_message");
        $this->db->where("first_time_active", 1);
        $this->db->where("tbl_appsettings.minimal_amount <", $amount);
        $this->db->from("tbl_appsettings");
        return $this->db->get()->row();
    }

    function checkActive() {
        $this->db->select("tbl_appsettings.id, tbl_appsettings.policy, tbl_appsettings.first_time_active, tbl_appsettings.first_time_percentage");
        $this->db->select("tbl_appsettings.first_time_amount, tbl_appsettings.minimal_amount, tbl_appsettings.initial_message");
        $this->db->where("first_time_active", 1);
        $this->db->from("tbl_appsettings");
        return $this->db->get()->row();
    }

}
