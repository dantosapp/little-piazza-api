<?php

class Countries_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getAll() {
        $this->db->select("id, name, code");
        $this->db->from("tbl_countries");
        return $this->db->get()->result();
    }

}