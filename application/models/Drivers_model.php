<?php

class Drivers_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function getDriverByEmail($email) {
        $this->db->select("tbl_drivers.cust_id, tbl_drivers.cust_name, tbl_drivers.cust_mobphone");
        $this->db->where("tbl_drivers.active", 1);
        $this->db->where("tbl_drivers.cust_emailaddress", $email);
        $this->db->from("tbl_drivers");
        return $this->db->get()->row();
    }

    function login($email, $password) {
        $this->db->select("tbl_drivers.cust_id, tbl_drivers.cust_name, tbl_drivers.cust_mobphone, tbl_drivers.cust_emailaddress");
        $this->db->where("tbl_drivers.password", $password);
        $this->db->where('tbl_drivers.cust_emailaddress', $email);
        $this->db->where('tbl_drivers.active', 1);
        $this->db->from('tbl_drivers');
        return $this->db->get()->row();
    }

    function updatePassword($email, $data) {
        $this->db->where("cust_emailaddress", $email);
        $this->db->update("tbl_drivers", $data);
        return $this->db->affected_rows();
    }




}
