<?php

class Customeraddress_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }


    function register($data) {
        $this->db->trans_begin();
        if ($data["adr_id"] > 0) {

            $this->db->where("adr_custid", $data["adr_custid"]);
            $this->db->where("adr_id", $data["adr_id"]);
            $this->db->update("tbl_customeraddress", $data);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $answer = array("success" => true, "customer" => $data["adr_id"]);
            } else {
                $this->db->trans_commit();
                $answer = array("success" => true, "customer" => $data["adr_id"]);
            }
        } else {
            $this->db->insert("tbl_customeraddress", $data);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $answer = array("success" => false, "message" => "Can't finish the registering process, please try again");
            } else {
                $record = $this->db->insert_id();
                if ($record > 0) {
                    $this->db->trans_commit();
                    $answer = array("success" => true, "customer" => $record);
                } else {
                    $this->db->trans_rollback();
                    $answer = array("success" => false, "message" => "Can't finish the registering process, please try again");
                }
            }
        }
        return $answer;
    }

    function getAddressByCustomer($cust_id) {
        $this->db->select("tbl_customeraddress.adr_id, tbl_customeraddress.adr_custid, tbl_customeraddress.adr_suburb");
        $this->db->select("tbl_customeraddress.adr_streetname, tbl_customeraddress.adr_unitnumber, tbl_customeraddress.adr_streetnumber");
        $this->db->select("tbl_customeraddress.adr_neareststreet");
        $this->db->select("tbl_suburbs.sbrb_deliveryfee, tbl_suburbs.sbrb_deliveryfreelimit");
        $this->db->where("tbl_customeraddress.adr_isactive", 1);
        $this->db->where("tbl_customeraddress.adr_custid", $cust_id);
        $this->db->from("tbl_customeraddress");
        $this->db->join("tbl_suburbs", "tbl_suburbs.sbrb_id = tbl_customeraddress.adr_suburb");
        $this->db->join("tbl_suburbstreets", "tbl_suburbstreets.sbrc_sbrbid = tbl_customeraddress.adr_suburb");
        return $this->db->get()->result();
    }


}
