<?php

class Suburbs_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function getAll() {
        $this->db->select("tbl_suburbs.sbrb_id, tbl_suburbs.sbrb_deliveryfee, tbl_suburbs.sbrb_belowlimit");
        $this->db->select("tbl_suburbs.sbrb_deliveryfreelimit, tbl_suburbs.sbrb_name");
        $this->db->select("tbl_suburbs.sbrb_delivery, tbl_suburbs.sbrb_pickup");
        $this->db->from("tbl_suburbs");
        $this->db->order_by("tbl_suburbs.sbrb_name");
        return $this->db->get()->result();
    }


}
