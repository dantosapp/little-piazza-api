<?php

class Schedule_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getRecordById($id) {
        $this->db->select("*");
        $this->db->from("tbl_schedule");
        return $this->db->get()->row();
    }

    function updateRecord($data) {
        $this->db->where("id", $data["id"]);
        $this->db->update("tbl_schedule", $data);
    }

    function checkIfOpen($time, $columnOpen, $columnClose) {
        $this->db->select("id");
        $this->db->where("".$columnOpen." <= ", $time);
        $this->db->where("".$columnClose." >= ", $time);
        $this->db->from("tbl_schedule");
        return $this->db->get()->row();
    }

    function getScheduleByDay() {
        $this->db->select("*");
        $this->db->from("tbl_schedule");
        return $this->db->get()->row();
    }

}