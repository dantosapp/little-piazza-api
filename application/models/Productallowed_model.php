<?php

class Productallowed_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getOptionsByProduct($product_id) {
        $this->db->select("tbl_prodallowedoptions.pro_id, tbl_prodallowedoptions.pro_optionid");
        $this->db->select("tbl_prodoptions.opt_name, tbl_prodoptions.opt_price");
        $this->db->where("tbl_prodallowedoptions.pro_prodid", $product_id);
        $this->db->where("tbl_prodallowedoptions.pro_showinweb", 1);
        $this->db->from("tbl_prodallowedoptions");
        $this->db->join("tbl_prodoptions", "tbl_prodoptions.opt_id = tbl_prodallowedoptions.pro_optionid");
        return $this->db->get()->result();
    }
}