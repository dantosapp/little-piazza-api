<?php

class Date_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function getLocalTime() {
        $timezone = 'Australia/Sydney';  //perl: $timeZoneName = "MY TIME ZONE HERE";
        $date = new DateTime('now', new DateTimeZone($timezone));
        return $date->format('Y-m-d H:i:s');
    }

}