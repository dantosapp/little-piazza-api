<?php

class Category_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function getAll() {
        $this->db->select("tbl_categories.cat_id, tbl_categories.cat_name, tbl_categories.cate_prdgroup");
        $this->db->select("tbl_categories.cat_posposition, tbl_categories.cat_webposition");
        $this->db->select("tbl_productsgroups.prg_name");
        $this->db->from("tbl_categories");
        $this->db->join("tbl_productsgroups", "tbl_productsgroups.prg_id = tbl_categories.cate_prdgroup");
        $this->db->order_by("tbl_categories.cate_prdgroup");
        return $this->db->get()->result();
    }

    function getRecordById($id) {
        $this->db->select("tbl_categories.cat_id, tbl_categories.cat_name, tbl_categories.cate_prdgroup");
        $this->db->select("tbl_categories.cat_posposition, tbl_categories.cat_webposition");
        $this->db->select("tbl_productsgroups.prg_name");
        $this->db->select("tbl_category_picture.photo, tbl_category_picture.icon");
        $this->db->where("tbl_categories.cat_id", $id);
        $this->db->from("tbl_categories");
        $this->db->join("tbl_productsgroups", "tbl_productsgroups.prg_id = tbl_categories.cate_prdgroup");
        $this->db->join("tbl_category_picture", "tbl_category_picture.category_id = tbl_categories.cat_id", "left");
        $this->db->order_by("tbl_categories.cate_prdgroup");
        return $this->db->get()->row();
    }

    function getActiveDeliveryRecords() {
        $this->db->select("tbl_categories.cat_id, tbl_categories.cat_name, tbl_categories.cate_prdgroup");
        $this->db->select("tbl_categories.cat_posposition, tbl_categories.cat_webposition");
        $this->db->where("tbl_productsgroups.prg_enableorderpage", 1);
        $this->db->from("tbl_categories");
        $this->db->join("tbl_productsgroups", "tbl_productsgroups.prg_id = tbl_categories.cate_prdgroup");
        $this->db->order_by("tbl_categories.cat_webposition");
        return $this->db->get()->result();
    }

    function getCategoriesWithHalf() {
        $this->db->select("distinct(tbl_categories.cat_id), tbl_categories.cat_name, tbl_categories.cate_prdgroup");
        $this->db->select("tbl_categories.cat_posposition, tbl_categories.cat_webposition");
        $this->db->where("tbl_productsgroups.prg_enableorderpage", 1);
        $this->db->where("tbl_products.prd_allowhalfandhalf", 1);
        $this->db->where("tbl_products.prd_showonweb", 1);
        $this->db->from("tbl_categories");
        $this->db->join("tbl_products", "tbl_products.prd_categoryid = tbl_categories.cat_id");
        $this->db->join("tbl_productsgroups", "tbl_productsgroups.prg_id = tbl_categories.cate_prdgroup");
        $this->db->order_by("tbl_categories.cat_webposition");
        return $this->db->get()->result();
    }

    function create($data, $files) {
        $this->load->model("CategoryPicture_model");

        $id = $data["cat_id"];
        unset($data["cat_id"]);
        $today = $data["updated_at"];
        unset($data["updated_at"]);
        
        if ($id > 0) {
            $record = $this->CategoryPicture_model->getRecordByCategory($id);
            
            $categoryPhoto = $this->CategoryPicture_model->create($files);
            if (strlen($categoryPhoto) > 0) {
                if (isset($record->photo) && strlen($record->photo) > 5) {
                    $this->CategoryPicture_model->deleteImageRecord($record->photo);
                }
                $this->CategoryPicture_model->updateRecordPicture($id, $categoryPhoto, $today);
            }

            $categoryIcon = $this->CategoryPicture_model->createIcon($files);
            if (strlen($categoryIcon) > 0) {
                if (isset($record->icon) && strlen($record->icon) > 5) {
                    $this->CategoryPicture_model->deleteImageRecord($record->icon);
                }
                $this->CategoryPicture_model->updateRecordIcon($id, $categoryIcon, $today);
            }

            $row = $this->update($data, $id);
        } else {
            $row = $this->insert($data);
        }
        return $row;
    }

    function insert($data) {
        $this->db->insert("tbl_categories", $data);
        return $this->db->insert_id();
    }

    function update($data, $id) {
        $this->db->where("cat_id", $id);
        $this->db->update("tbl_categories", $data);
        return $this->db->affected_rows();
    }
}
