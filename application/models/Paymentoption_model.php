<?php

class Paymentoption_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getById($id) {
        $this->db->select("pay_id, pay_merchantuid, pay_apikey, pay_apipasspharse, pay_percentagefee");
        $this->db->where("pay_id", $id);
        $this->db->from("tbl_paymentoptions");
        return $this->db->get()->row();
    }

    function getByProvider($provider_id) {
        $this->db->select("tbl_paymentoptions.pay_id, tbl_paymentoptions.pay_merchantuid, tbl_paymentoptions.pay_apikey");
        $this->db->select("tbl_paymentoptions.pay_apipasspharse, tbl_paymentoptions.pay_percentagefee");
        $this->db->select("tbl_paymentproviders.pymt_providername");
        $this->db->where("tbl_paymentoptions.pay_providerid", $provider_id);
        $this->db->join("tbl_paymentproviders", "tbl_paymentproviders.pymt_apiid = tbl_paymentoptions.pay_providerid");
        $this->db->from("tbl_paymentoptions");
        return $this->db->get()->row();
    }

    function processPayment($postData) {

        // Setup the POST url
//        define('MW_API_ENDPOINT', 'https://base.merchantwarrior.com/post/');
        define('MW_API_ENDPOINT', 'https://api.merchantwarrior.com/post/');

        $postData['hash'] = $this->calculateHash($postData);

        // Setup CURL defaults
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_TIMEOUT, 60);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($curl, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($curl, CURLOPT_FORBID_REUSE, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        // Setup CURL params for this request
        curl_setopt($curl, CURLOPT_URL, MW_API_ENDPOINT);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postData, '', '&'));

        // Run CURL
        $response = curl_exec($curl);
        $error = curl_error($curl);

        // Check for CURL errors
        if (isset($error) && strlen($error))
        {
            throw new Exception("CURL Error: {$error}");
        }

        // Make sure the API returned something
        if (!isset($response) || strlen($response) < 1)
        {
            throw new Exception("API response was empty");
        }

        // Parse the XML
        $xml = simplexml_load_string($response);
        // Convert the result from a SimpleXMLObject into an array
        $xml = (array)$xml;

        // Check for a valid response code
        if (!isset($xml['responseCode']) || strlen($xml['responseCode']) < 1)
        {
            throw new Exception("API Response did not contain a valid responseCode");
        }

        // Validate the response - the only successful code is 0
        $status = ((int)$xml['responseCode'] === 0) ? true : false;

        // Make the response a little more useable
        $result = array('status' => $status, 'transactionID' => (isset($xml['transactionID']) ? $xml['transactionID'] : null), 'responseData' => $xml);

        // If you don't have xdebug, you can echo a <pre> and then exit(print_r($result, true)); below instead of the var_dump.
        //exit(var_dump($result));
        return $result;
    }

    /**
     * Generates and returns the request hash after being
     * provided with the postData array.
     * @param array $postData
     * @return string
     */
    private function calculateHash(array $postData = array())
    {
        // Check the amount param
        if (!isset($postData['transactionAmount']) || !strlen($postData['transactionAmount']))
        {
            exit("Missing or blank amount field in postData array.");
        }

        // Check the currency param
        if (!isset($postData['transactionCurrency']) || !strlen($postData['transactionCurrency']))
        {
            exit("Missing or blank currency field in postData array.");
        }

        // Generate & return the hash
        return md5(strtolower(MW_API_PASS_PHRASE. MW_MERCHANT_UUID. $postData['transactionAmount'] . $postData['transactionCurrency']));
    }

    function validatePaypalToken($paymentID) {
//        curl -v -X GET https://api.sandbox.paypal.com/v1/payments/payment/PAY-5YK922393D847794YKER7MUI \
//-H "Content-Type:application/json" \
//        -H "Authorization: Bearer Access-Token"

        // Setup the POST url
        define('PAYPAL_API_ENDPOINT', 'https://api.sandbox.paypal.com/v1/payments/payment/'.$paymentID.'');

        $header = array();
        $header[] = 'Content-type: application/json';
        $header[] = 'Authorization: Bearer A101.YQllm3zRkOGpCBZc1FMIWAGsjYRxjd1ItAB6xeCH2x2Iwi2yug-yei6_n6gRAUMS.S8uq5y06cfYclQ-zwqqnQk1mmJa';


        // Setup CURL defaults
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_TIMEOUT, 60);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($curl, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($curl, CURLOPT_FORBID_REUSE, true);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        // Setup CURL params for this request
        curl_setopt($curl, CURLOPT_URL, PAYPAL_API_ENDPOINT);
        curl_setopt($curl, CURLOPT_POST, false);

        // Run CURL
        $response = curl_exec($curl);
        $error = curl_error($curl);

        // Check for CURL errors
        if (isset($error) && strlen($error))
        {
            throw new Exception("CURL Error: {$error}");
        }

        // Make sure the API returned something
        if (!isset($response) || strlen($response) < 1)
        {
            throw new Exception("API response was empty");
        }

        // Parse the XML
        print $response;
        $xml = simplexml_load_string($response);
        // Convert the result from a SimpleXMLObject into an array
        $xml = (array)$xml;

        // Check for a valid response code
        if (!isset($xml['responseCode']) || strlen($xml['responseCode']) < 1)
        {
            throw new Exception("API Response did not contain a valid responseCode");
        }

        // Validate the response - the only successful code is 0
        $status = ((int)$xml['responseCode'] === 0) ? true : false;

        // Make the response a little more useable
        $result = array('status' => $status, 'transactionID' => (isset($xml['transactionID']) ? $xml['transactionID'] : null), 'responseData' => $xml);

        // If you don't have xdebug, you can echo a <pre> and then exit(print_r($result, true)); below instead of the var_dump.
        //exit(var_dump($result));
        return $result;
    }

}