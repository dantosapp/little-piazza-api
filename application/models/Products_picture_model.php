<?php

class Products_picture_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function configFiles() {
        //upload an image options
        $config = array();
        $config['upload_path']      = './assets/img/products';
        $config['create_thumb']     = true;
        $config['allowed_types']    = 'gif|jpg|png';
        $config['max_size']	        = 22000;
        $config['image_width']      = 1024;
        $config['image_height']     = 768;
        $config['maintain_ratio']   = true;
        $config['encrypt_name']     = TRUE;
        return $config;
    }

    function getRecord($prp_prodid) {
        $this->db->select("prp_id, prp_prodid, prp_pictureatpath");
        $this->db->where("prp_prodid", $prp_prodid);
        $this->db->from("tbl_productpictures");
        return $this->db->get()->row();
    }

    function deleteImageRecord($name) {
        unlink("assets/img/products/".$name);
        unlink("assets/img/products/mobile/".$name);
    }

    function updateRecordPicture($prp_prodid, $name) {
        $data = array("prp_pictureatpath" => $name);
        $this->db->where("prp_prodid", $prp_prodid);
        $this->db->update("tbl_productpictures", $data);
    }

    function newRecord($data) {
        $this->db->insert("tbl_productpictures", $data);
    }

    function create($data, $file) {
        if (isset($data) && count($data) != 0) {
            $this->load->library('upload');
            $files = $file;
            if (isset($files['photo'])) {
                $_FILES = '';
                $_FILES['userfile']['name']     = $files['photo']['name'];
                $_FILES['userfile']['type']     = $files['photo']['type'];
                $_FILES['userfile']['tmp_name'] = $files['photo']['tmp_name'];
                $_FILES['userfile']['error']    = $files['photo']['error'];
                $_FILES['userfile']['size']     = $files['photo']['size'];

                $this->upload->initialize($this->configFiles());
                $this->upload->do_upload();
                $dataPicture = $this->upload->data();

                $record = $this->getRecord($data["prd_id"]);
                if (!empty($record)) {
                    $this->updateRecordPicture($data["prd_id"], $dataPicture["file_name"]);
                    $this->deleteImageRecord($record->prp_pictureatpath);
                } else {
                    $data = array(
                        "prp_prodid"        => $data["prd_id"],
                        "prp_pictureatpath" => $dataPicture["file_name"]
                    );
                    $this->newRecord($data);
                }

                $this->createMobileFile($dataPicture["full_path"]);
            }
            return array("success" => true);
        } else {
            return array("success" => false, "message" => "Please select a valid picture");
        }
    }

    function createMobileFile($path) {
        $config = array(
            'source_image'      => $path, //path to the uploaded image
            'new_image'         => './assets/img/products/mobile/', //path to
            'maintain_ratio'    => true,
            //'width'             => 300,
            'height'            => 400
        );
        $this->load->library('image_lib');
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }
}
