<?php

class Product_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function getPriceProductSize($prd_id, $size) {
        $this->db->select("prd_".$size."price as price");
        $this->db->where("tbl_products.prd_id", $prd_id);
        $this->db->from("tbl_products");
        return $this->db->get()->row();
    }

    function getProductsWithHalf() {
        $this->db->select("tbl_products.prd_id, tbl_products.prd_restid, tbl_products.prd_name, tbl_products.prd_description, tbl_products.prd_groupid, tbl_products.prd_categoryid");
        $this->db->select("tbl_products.prd_smallprice, tbl_products.prd_mediumprice, tbl_products.prd_largeprice, tbl_products.prd_customprice");
        $this->db->select("tbl_products.prd_fixedprice, tbl_products.prd_basetype, tbl_products.prd_allowhalfandhalf, tbl_products.prd_extratoppingsallowed");
        $this->db->select("tbl_products.prd_extradeliveryfee, tbl_productpictures.prp_pictureatpath as picture");
        $this->db->where("tbl_products.prd_allowhalfandhalf", 1);
        $this->db->where("tbl_products.prd_showonweb", 1);
        $this->db->from("tbl_products");
        $this->db->join("tbl_productpictures", "tbl_productpictures.prp_prodid = tbl_products.prd_id", "left");
        return $this->db->get()->result();
    }

    function getProductsByCategory($category_id) {
        $this->db->select("tbl_products.prd_id, tbl_products.prd_restid, tbl_products.prd_name, tbl_products.prd_description, tbl_products.prd_groupid, tbl_products.prd_categoryid");
        $this->db->select("tbl_products.prd_smallprice, tbl_products.prd_mediumprice, tbl_products.prd_largeprice, tbl_products.prd_customprice");
        $this->db->select("tbl_products.prd_fixedprice, tbl_products.prd_basetype, tbl_products.prd_allowhalfandhalf, tbl_products.prd_extratoppingsallowed");
        $this->db->select("tbl_products.prd_extradeliveryfee, tbl_productpictures.prp_pictureatpath as picture");
        $this->db->where("tbl_products.prd_categoryid", $category_id);
        $this->db->where("tbl_products.prd_groupid", 1);
        $this->db->where("tbl_products.prd_showonweb", 1);
        $this->db->from("tbl_products");
        $this->db->join("tbl_productpictures", "tbl_productpictures.prp_prodid = tbl_products.prd_id", "left");
        return $this->db->get()->result();
    }

    function getAllDelivery() {
        $this->db->select("tbl_products.prd_id, tbl_products.prd_name, tbl_products.prd_description, tbl_products.prd_groupid, tbl_products.prd_categoryid");
        $this->db->select("tbl_products.prd_smallprice, tbl_products.prd_mediumprice, tbl_products.prd_largeprice, tbl_products.prd_customprice");
        $this->db->select("tbl_products.prd_fixedprice, tbl_products.prd_basetype, tbl_products.prd_allowhalfandhalf, tbl_products.prd_extratoppingsallowed");
        $this->db->select("tbl_products.prd_extradeliveryfee, tbl_productpictures.prp_pictureatpath as picture");
        $this->db->where("tbl_productsgroups.prg_enableorderpage", 1);
        $this->db->from("tbl_products");
        $this->db->join("tbl_productpictures", "tbl_productpictures.prp_prodid = tbl_products.prd_id", "left");
        $this->db->join("tbl_productsgroups", "tbl_productsgroups.prg_id = tbl_products.prd_groupid");
        $this->db->order_by("tbl_products.prd_name");
        return $this->db->get()->result();
    }

    function getAll() {
        $this->db->select("tbl_products.prd_id, tbl_products.prd_name, tbl_products.prd_description, tbl_products.prd_groupid, tbl_products.prd_categoryid");
        $this->db->select("tbl_products.prd_smallprice, tbl_products.prd_mediumprice, tbl_products.prd_largeprice, tbl_products.prd_customprice");
        $this->db->select("tbl_products.prd_fixedprice, tbl_products.prd_basetype, tbl_products.prd_allowhalfandhalf, tbl_products.prd_extratoppingsallowed");
        $this->db->select("tbl_products.prd_extradeliveryfee, tbl_categories.cat_name AS category, tbl_productsgroups.prg_name AS group_name");
        $this->db->from("tbl_products");
        $this->db->join("tbl_productsgroups", "tbl_productsgroups.prg_id = tbl_products.prd_groupid");
        $this->db->join("tbl_categories", "tbl_categories.cat_id = tbl_products.prd_categoryid");
        $this->db->order_by("tbl_productsgroups.prg_id");
        return $this->db->get()->result();
    }

    function getRecord($prd_id) {
        $this->db->select("tbl_products.prd_id, tbl_products.prd_name, tbl_products.prd_description, tbl_products.prd_groupid, tbl_products.prd_categoryid");
        $this->db->select("tbl_products.prd_smallprice, tbl_products.prd_mediumprice, tbl_products.prd_largeprice, tbl_products.prd_customprice");
        $this->db->select("tbl_products.prd_fixedprice, tbl_products.prd_basetype, tbl_products.prd_allowhalfandhalf, tbl_products.prd_extratoppingsallowed");
        $this->db->select("tbl_products.prd_extradeliveryfee, tbl_categories.cat_name AS category, tbl_productsgroups.prg_name AS group_name");
        $this->db->select("tbl_productpictures.prp_pictureatpath as picture");
        $this->db->where("tbl_products.prd_id", $prd_id);
        $this->db->from("tbl_products");
        $this->db->join("tbl_productsgroups", "tbl_productsgroups.prg_id = tbl_products.prd_groupid");
        $this->db->join("tbl_categories", "tbl_categories.cat_id = tbl_products.prd_categoryid");
        $this->db->join("tbl_productpictures", "tbl_productpictures.prp_prodid = tbl_products.prd_id", "left");
        return $this->db->get()->row();
    }

    function getRecordByIdByGroup($prd_id, $prd_group) {
        $this->db->select("tbl_products.prd_id, tbl_products.prd_name, tbl_products.prd_description, tbl_products.prd_groupid, tbl_products.prd_categoryid");
        $this->db->select("tbl_products.prd_smallprice, tbl_products.prd_mediumprice, tbl_products.prd_largeprice, tbl_products.prd_customprice");
        $this->db->select("tbl_products.prd_fixedprice, tbl_products.prd_basetype, tbl_products.prd_allowhalfandhalf, tbl_products.prd_extratoppingsallowed");
        $this->db->select("tbl_products.prd_extradeliveryfee, tbl_categories.cat_name AS category, tbl_productsgroups.prg_name AS group_name");
        $this->db->select("tbl_productpictures.prp_pictureatpath as picture");
        $this->db->where("tbl_products.prd_id", $prd_id);
        $this->db->where("tbl_products.prd_groupid", $prd_group);
        $this->db->from("tbl_products");
        $this->db->join("tbl_productsgroups", "tbl_productsgroups.prg_id = tbl_products.prd_groupid");
        $this->db->join("tbl_categories", "tbl_categories.cat_id = tbl_products.prd_categoryid");
        $this->db->join("tbl_productpictures", "tbl_productpictures.prp_prodid = tbl_products.prd_id", "left");
        return $this->db->get()->row();
    }

    function getProductByIdByCategory($prd_id, $prd_categoryid) {
        $this->db->select("tbl_products.prd_id, tbl_products.prd_name, tbl_products.prd_description, tbl_products.prd_groupid, tbl_products.prd_categoryid");
        $this->db->select("tbl_products.prd_smallprice, tbl_products.prd_mediumprice, tbl_products.prd_largeprice, tbl_products.prd_customprice");
        $this->db->select("tbl_products.prd_fixedprice, tbl_products.prd_basetype, tbl_products.prd_allowhalfandhalf, tbl_products.prd_extratoppingsallowed");
        $this->db->where("tbl_products.prd_id", $prd_id);
        $this->db->where("tbl_products.prd_categoryid", $prd_categoryid);
        $this->db->from("tbl_products");
        return $this->db->get()->row();
    }

    function getProductByArrayIdByCategory($prd_id, $prd_categoryid) {
        $this->db->select("tbl_products.prd_id, tbl_products.prd_name, tbl_products.prd_description, tbl_products.prd_groupid, tbl_products.prd_categoryid");
        $this->db->select("tbl_products.prd_smallprice, tbl_products.prd_mediumprice, tbl_products.prd_largeprice, tbl_products.prd_customprice");
        $this->db->select("tbl_products.prd_fixedprice, tbl_products.prd_basetype, tbl_products.prd_allowhalfandhalf, tbl_products.prd_extratoppingsallowed");
        $this->db->where_in("tbl_products.prd_id", $prd_id);
        $this->db->where("tbl_products.prd_categoryid", $prd_categoryid);
        $this->db->order_by("tbl_products.prd_fixedprice", "desc");
        $this->db->from("tbl_products");
        $this->db->limit("1");
        return $this->db->get()->row();
    }


}
