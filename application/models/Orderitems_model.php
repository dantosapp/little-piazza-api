<?php

class Orderitems_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function saveOrder($data) {
        $this->db->insert("tbl_orderitems", $data);
        return $this->db->insert_id();
    }

    function updateType($id, $type) {
        $this->db->where("ori_id", $id);
        $this->db->update("tbl_orderitems", $type);
    }

    function getItemsByOrder($order_id) {
        $this->db->select("tbl_orderitems.ori_id, tbl_orderitems.ori_prodid, tbl_orderitems.ori_price, tbl_orderitems.ori_quantity, tbl_orderitems.ori_basetype");
        $this->db->select("tbl_orderitems.ori_selectedoption");
        $this->db->select("tbl_basetypes.bse_name as base_name");
        $this->db->select("tbl_products.prd_name, tbl_products.prd_showonweb");
        $this->db->select("tbl_orderitems.ori_halfandhalf, tbl_orderitems.ori_halfandhalfwith, tbl_orderitems.ori_sizeselection");
        $this->db->where("tbl_orderitems.ori_orderid", $order_id);
        $this->db->from("tbl_orderitems");
        $this->db->join("tbl_products", "tbl_products.prd_id = tbl_orderitems.ori_prodid");
        $this->db->join("tbl_basetypes", "tbl_basetypes.bse_id = tbl_orderitems.ori_basetype");
        return $this->db->get()->result();
    }


}