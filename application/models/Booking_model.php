<?php

class Booking_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function sendBookingMessage($message, $name) {
        $from_email = "no-reply@littlepiazza.com.au";
        //Load email library
        $this->load->library('email');

        $this->email->set_mailtype("html");
        $this->email->from($from_email, $name);
        $this->email->to("info@littlepiazza.com.au");
        $this->email->subject($name);
        $this->email->message($message);

        //Send mail
        $this->email->send();
    }

    function storeBooking($data) {
        $this->db->insert("tbl_booking", $data);
        return $this->db->insert_id();
    }

    function bookingResponse($data) {

        $sureName = (isset($data["surname"]) ? $data["surname"] : "");
        $behalf = (isset($data["booking_name"]) ? "Your reservation name is for <strong>".$data["booking_name"]."</strong>" : "");
        $special = (strlen($data["special_request"] > 0) ? "Your Special Request is  <strong>".$data["special_request"]."</strong>" : "");

        if ($data["time"] > "00:00" && $data["time"] < "17:00") {
            $serviceType = "lunch";
        } else {
            $serviceType = "dinner";
        }

        $body = '
            <html>
                <body align="center">

                    <table width="70%" align="center" >
                        <tr>
                            <td>
                                <br>
                                Hi '.$data["name"].' '.$sureName.'
                                <br><br>
                                This is a confirmation to your reservation and therefore we may not contact you unless there is any problem.
                                <br><br>
                                You have reserved for <strong>'.$serviceType.'</strong>  for <strong>'.$data["time"].'</strong> on  <strong>'.$data["date"].'</strong> for '.$data["quests"].'.
                                <br><br>
                                '.$behalf.'
                                <br><br>
                                '.$special.'
                                <br>
                                Please Call us directly on 02 9662 1227
                                <ul>
                                    <li>
                                        if your reservation is for more than 10 people
                                    </li>
                                    <li>
                                        If your booking time is within 24 hours
                                    </li>
                                </ul>
                                <br>
                                Thank you and we will see you on <strong>'.$data["date"].'</strong> at <strong>'.$data["time"].'</strong>
                                <br>
                                <br>
                                <strong>With Regards</strong>
                                <br>
                                <strong>Little Piazza</strong>
                                <br>

                            </td>
                        </tr>
                    </table>
                    <table width="70%" align="center" >
                        <tr>
                            <td><a href="http://www.littlepiazza.com.au"><img width="50px" height="50px" src="http://www.littlepiazza.com.au/images/logo.png"></a></td>

                        </tr>
                    </table>
                </body>
            </html>
        ';
        return $body;
    }

    function bookingBody($data) {
        $sureName = (isset($data["surname"]) ? $data["surname"] : "");
        $behalf = (isset($data["booking_name"]) ? 'On behalf: '.$data["booking_name"].'' : "");
        $meal = (isset($data["meal"]) ? 'For: '.$data["meal"].'' : "");
        $device = (isset($data["device"]) ? $data["device"] : "mobile App");
        $body = '
            <html>
                <body align="center">
                    <table width="70%" align="center" style="background-color: lightgrey;">
                        <tr>
                            <td><a href="http://www.littlepiazza.com.au"><img src="http://www.littlepiazza.com.au/images/logo.png"></a></td>
                            <td align="right"><span style="background-color:#3498db;border: 10px solid #3498db;color:#fff;">Little Piazza</spam></td>
                        </tr>
                    </table>
                    <table width="70%" align="center" >
                        <tr>
                            <td>
                                <br>
                                '.$data["name"].' '.$sureName.', has requested a booking service from the '.$device.'
                                <br>
                                Email: <strong>'.$data["email"].'</strong>
                                <br>
                                Phone: <strong>'.$data["mobile"].'</strong>
                                <br>
                                The request is:
                                <br>
                                '.$meal.'
                                <br>
                                Number of Guests: '.$data["quests"].'
                                <br>
                                Date: <strong>'.$data["date"].'</strong> at <strong>'.$data["time"].'</strong>
                                <br>
                                '.$behalf.'
                                <br>
                                Special request: '.$data["special_request"].'
                                <br>

                                <br>
                            </td>
                        </tr>
                    </table>
                    <table width="70%" align="center" style="background-color: lightgrey;">
                        <tr>
                            <td style="border: 15px solid lightgray;">Little Piazza</td>
                        </tr>
                    </table>
                </body>
            </html>
        ';
        return $body;
    }


}
