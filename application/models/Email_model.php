<?php
class Email_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }



    function sendEmail($to_email, $message) {
        $from_email = "no-reply@littlepiazza.com.au";
        //Load email library
        $this->load->library('email');

        $this->email->set_mailtype("html");
        $this->email->from($from_email, "Little Piazza");
        $this->email->to($to_email);
        $this->email->subject('Message From Little Piazza');
        $this->email->message($message);

        //Send mail
        $this->email->send();
    }


    //*** BODY EMAILS
    function contactBody($data) {
        $body = '
            <html>
                <body align="center">
                    <table width="70%" align="center" style="background-color: lightgrey;">
                        <tr>
                            <td><a href="http://www.littlepiazza.com.au"><img src="http://www.littlepiazza.com.au/images/logo.png"></a></td>
                            <td align="right"><span style="background-color:#3498db;border: 10px solid #3498db;color:#fff;">Little Piazza</spam></td>
                        </tr>
                    </table>
                    <table width="70%" align="center" >
                        <tr>
                            <td>
                                <br>
                                Hello, '.$data["name"].' has sent you a contact message.
                                <br>
                                Email: '.$data["email"].'
                                <br>
                                Phone Number: '.$data["phone"].'
                                <br>
                                Message:
                                <br>
                                '.$data["comment"].'

                                <br>

                                <br>
                            </td>
                        </tr>
                    </table>
                    <table width="70%" align="center" style="background-color: lightgrey;">
                        <tr>
                            <td style="border: 15px solid lightgray;">Little Piazza</td>
                        </tr>
                    </table>
                </body>
            </html>
        ';
        return $body;
    }

    function forgotPasswordBody($data) {
        $customer = MD5($data->cust_id);
        $body = '
            <html>
                <body align="center">
                    <table width="70%" align="center" style="background-color: lightgrey;">
                        <tr>
                            <td><a href="http://www.littlepiazza.com.au"><img src="http://www.littlepiazza.com.au/images/logo.png"></a></td>
                            <td align="right"><span style="background-color:#3498db;border: 10px solid #3498db;color:#fff;">Little Piazza</spam></td>
                        </tr>
                    </table>
                    <table width="70%" align="center" >
                        <tr>
                            <td>
                                <br>
                                Hello '.$data->cust_name.' '.$data->cust_surname.', you requested a password change.
                                <br>
                                Please use the following link and process your request. You have 24 hours to compelte it.
                                <br>
                                <a href="'.base_url("Forgot_password?token=$data->token&customer=$customer").'">'.base_url("Forgot_password?token=$data->token&customer=$customer").'</a>

                                <br>

                                <br>
                                Best regards...
                                <br>
                            </td>
                        </tr>
                    </table>
                    <table width="70%" align="center" style="background-color: lightgrey;">
                        <tr>
                            <td style="border: 15px solid lightgray;">Little Piazza</td>
                        </tr>
                    </table>
                </body>
            </html>
        ';
        return $body;
    }

    function clientRegisterBody($data) {
        $body = '
            <html>
                <body align="center">
                    <table width="70%" align="center" style="background-color: lightgrey;">
                        <tr>
                            <td><a href="http://wsti.com.ar/"><img src="'.base_url("assets/img/logo_wsti.jpg").'"></a></td>
                            <td align="right"><span style="background-color:#3498db;border: 10px solid #3498db;color:#fff;">Bienvenido</spam></td>
                        </tr>
                    </table>
                    <table width="70%" align="center" >
                        <tr>
                            <td>
                                <br>
                                Hola '.$data["name"].', bienvenid@ al Sistema múltiple de WSTI Argentina.
                                <br>
                                Sus dataos de acceso son:
                                <br>
                                Email: <strong>'.$data["email"].'</strong>
                                <br>
                                Contraseña: <strong>'.$data["password"].'</strong>
                                <br>
                                <br>
                                Te recomendamos por seguridad cambiar la contraseña en el área de perfil.
                                <br>
                                Puedes ingresar a su panel de control a trvés del siguiente enlace:
                                <br>
                                <a href="'.base_url("").'">'.base_url().'</a>
                                <br>
                                Gracias por utilizar nuestros servicios
                                <br>
                            </td>
                        </tr>
                    </table>
                    <table width="70%" align="center" style="background-color: lightgrey;">
                        <tr>
                            <td style="border: 15px solid lightgray;">Wsti Argentina</td>
                        </tr>
                    </table>
                </body>
            </html>
        ';
        return $body;
    }

    function registerProductClient($service, $code) {
        $body = '
            <html>
                <body align="center">
                    <table width="70%" align="center" style="background-color: lightgrey;">
                        <tr>
                            <td><a href="http://wsti.com.ar/"><img src="'.base_url("assets/img/logo_wsti.jpg").'"></a></td>
                            <td align="right"><span style="background-color:#3498db;border: 10px solid #3498db;color:#fff;">Bienvenido</spam></td>
                        </tr>
                    </table>
                    <table width="70%" align="center" >
                        <tr>
                            <td>
                                <br>
                                Se ha registrado un nuevo producto de <strong>'.$service.'</strong> con la siguiente clave de acceso:
                                <br>
                                <h2>'.$code.'</h2>
                                <br>
                                Esta clave es el identificador único para su aplicación.
                                <br>
                                Puede ingresar a su panel de control utilizando el siguiente enlace:
                                <br>
                                <a href="'.base_url("").'">'.base_url().'</a>
                                <br>
                                Gracias por utilizar nuestros servicios
                                <br>
                            </td>
                        </tr>
                    </table>
                    <table width="70%" align="center" style="background-color: lightgrey;">
                        <tr>
                            <td style="border: 15px solid lightgray;">Wsti Argentina</td>
                        </tr>
                    </table>
                </body>
            </html>
        ';
        return $body;
    }


}
