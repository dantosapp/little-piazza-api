<?php

class Ordertopping_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function saveOrder($data) {
        $this->db->insert("tbl_ordertopping", $data);
        return $this->db->insert_id();
    }

    function getItemsByDetail($orderDetail_id) {
        $this->db->select("tbl_ordertopping.oro_id, tbl_ordertopping.oro_productid, tbl_ordertopping.oro_optype, tbl_ordertopping.oro_name, tbl_ordertopping.oro_price");
        $this->db->where("tbl_ordertopping.oro_orderdetailid", $orderDetail_id);
        $this->db->from("tbl_ordertopping");
        return $this->db->get()->result();
    }

}
