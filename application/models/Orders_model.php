<?php

class Orders_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function getOrderDetailsByID($id) {
        $this->db->select("tbl_orders.ord_id, tbl_orders.ord_number, ord_creditcardno");
        $this->db->where("tbl_orders.ord_id", $id);
        $this->db->from("tbl_orders");
        return $this->db->get()->row();
    }

    function getOrderByNumber($number) {
        $this->db->select("tbl_orders.ord_id");
        $this->db->where("tbl_orders.ord_number", $number);
        $this->db->from("tbl_orders");
        return $this->db->get()->row();
    }

    function getOrderByCustomer($customer_id) {
        $this->db->select("ord_id");
        $this->db->where("ord_customerid", $customer_id);
        $this->db->from("tbl_orders");
        return $this->db->get()->row();
    }

    function getOrdersByCustomer($customer_id) {
        $this->db->select("tbl_orders.ord_id, tbl_orders.ord_number, tbl_orders.ord_subtotal as ord_grosstotal, tbl_orders.ord_createdate, tbl_orders.ord_customerid");
        $this->db->where("tbl_orders.ord_customerid", $customer_id);
        $this->db->from("tbl_orders");
        $this->db->order_by("tbl_orders.ord_id", "desc");
        $this->db->limit(5);
        return $this->db->get()->result();
    }


    function getOrderByCustomerMobile($customer_id) {
        $this->db->select("ord_id");
        $this->db->where("ord_customerid", $customer_id);
        $this->db->or_where("ord_from", "iOS_App");
        $this->db->or_where("ord_from", "And_App");
        $this->db->from("tbl_orders");
        return $this->db->get()->row();
    }

    function saveOrder($data) {
        $this->db->insert("tbl_orders", $data);
        return $this->db->insert_id();
    }


}
