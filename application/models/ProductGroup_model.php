<?php

class ProductGroup_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function getAll() {
        $this->db->select("tbl_productsgroups.prg_id, tbl_productsgroups.prg_name");
        $this->db->from("tbl_productsgroups");
        return $this->db->get()->result();
    }
}
