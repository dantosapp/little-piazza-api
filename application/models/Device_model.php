<?php

class Device_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function getDeviceByUserOs($customer_id, $os) {
        $this->db->select("tbl_device.id, tbl_device.token");
        $this->db->where("tbl_device.customer_id", $customer_id);
        $this->db->where("tbl_device.os", $os);
        $this->db->from("tbl_device");
        return $this->db->get()->row();
    }

    function create($data) {
        $this->db->insert("tbl_device", $data);
        return $this->db->insert_id();
    }

    function update($data) {
        $this->db->where("customer_id", $data["customer_id"]);
        $this->db->where("os", $data["os"]);
        $this->db->update("tbl_device", $data);
        return $this->db->affected_rows();
    }

    function sendAndroidPush($token, $title, $subtitle, $message, $attachment, $url, $notificationType) {
        $API_ACCESS_KEY =  'AAAAclGaBoA:APA91bEkMVmEoSXvlRussBBB0kQesHNm6N2G9CGFUyFKllh_FnQxztZTIqaAb5FKpqujp24uV68DcUso-e7eeEM1zS576o0GPNOOXO4ew6-xc7zYlW3pdq3T_HN5P6IkTdwMyZUqU_Ft';
        $registrationIds = array( $token );

        // prep the bundle
        $msg = array (
            'body' 	        => strip_tags($message),
            'title'		    => $title,
            'vibrate'	    => 1,
            'largeIcon'	    => 'large_icon',
            'smallIcon'	    => 'small_icon',
        );

        $extra = array(
            "type"              => $notificationType,
            "attachment-url"    => $attachment,
            "title"             => $title,
            "subtitle"          => $subtitle,
            'body'              => strip_tags($message),
            "message"           => $message,
            'largeIcon'	        => 'large_icon',
            'smallIcon'	        => 'small_icon',
            "url"               => $url
        );

        $fields = array (
            'registration_ids' 	=> $registrationIds,
            'data'              => $extra
        );

        $headers = array (
            'Authorization: key=' . $API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch);
       var_dump($result);
        curl_close( $ch );
    }

}
