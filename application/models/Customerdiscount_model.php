<?php

class Customerdiscount_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function validateDiscount($data) {

    }

    function getDiscount($code) {
        $this->db->select("tbl_discounts.disc_id, tbl_discounts.disc_percentage, tbl_discounts.disc_dicountfixed");
        $this->db->where("tbl_discounts.disc_couponcode", $code);
        $this->db->where("tbl_discounts.disc_isactive", 1);
        $this->db->where("tbl_discounts.disc_remained > ", 0);
        $this->db->from("tbl_discounts");
        return $this->db->get()->row();
    }

    function create($data) {
        $this->db->insert("tbl_acustomer_discount", $data);
    }


}