<?php

class Suburbstreets_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function getBySuburb($suburb_id) {
        $this->db->select("tbl_suburbstreets.sbrc_id, tbl_suburbstreets.sbrc_sbrbid, tbl_suburbstreets.sbrc_streetname");
        $this->db->where("tbl_suburbstreets.sbrc_sbrbid", $suburb_id);
        $this->db->from("tbl_suburbstreets");
        $this->db->order_by("tbl_suburbstreets.sbrc_streetname");
        return $this->db->get()->result();
    }


}
