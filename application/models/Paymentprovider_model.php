<?php

class Paymentprovider_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function getRecord($id) {
        $this->db->select("tbl_paymentoptions.pay_id, tbl_paymentoptions.pay_shortname");
        $this->db->where("tbl_paymentoptions.pay_id", $id);
        $this->db->from("tbl_paymentoptions");
        return $this->db->get()->row();
    }

}
