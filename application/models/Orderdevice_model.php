<?php

class Orderdevice_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function getRecorByCustomer($customer_id) {
        $this->db->select("tbl_order_device.id");
        $this->db->where("tbl_order_device.customer_id", $customer_id);
        $this->db->from("tbl_order_device");
        return $this->db->get()->row();
    }

    function getRecordByToken($token) {
        $this->db->select("tbl_order_device.id");
        $this->db->where("tbl_order_device.token", $token);
        $this->db->from("tbl_order_device");
        return $this->db->get()->row();
    }

    function create($data) {
        $this->db->insert("tbl_order_device", $data);
    }

    function getAll() {
        $this->db->select("distinct(tbl_order_device.customer_id) as customer_id, tbl_order_device.id, tbl_order_device.token, tbl_order_device.created_at");
        $this->db->select("tbl_orders.ord_number, tbl_orders.ord_id as orderId, tbl_orders.ord_deliverytype, tbl_orders.ord_grosstotal");
        $this->db->select("tbl_orders.ord_discounttotal");
        $this->db->select("tbl_device.os, tbl_customers.cust_name, tbl_customers.cust_surname, tbl_customers.cust_emailaddress, tbl_customers.cust_mobphone");
        $this->db->where("tbl_order_device.isFirstTime", 1);
        $this->db->from("tbl_order_device");
        $this->db->join("tbl_device", "tbl_device.token = tbl_order_device.token", "left");
        $this->db->join("tbl_customers", "tbl_customers.cust_id = tbl_order_device.customer_id");
        $this->db->join("tbl_orders", "tbl_orders.ord_id = tbl_order_device.order_id", "left");
        return $this->db->get()->result();
    }

}
