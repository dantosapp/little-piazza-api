<?php

class User_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function login($data) {
        $this->db->select("usr_name, usr_emailaddress");
        $this->db->where("usr_name", $data["usr_name"]);
        $this->db->where("usr_password", $data["usr_password"]);
        $this->db->from("tbl_users");
        return $this->db->get()->row();
    }

    function getUserByEmail($email) {
        $this->db->select("usr_name, usr_emailaddress");
        $this->db->where("usr_emailaddress", $email);
        $this->db->from("tbl_users");
        return $this->db->get()->row();
    }

    function getUserLoggingApi($email, $password) {
        $this->db->select("tbl_users.usr_name as cust_name, tbl_users.usr_emailaddress as cust_emailaddress");
        $this->db->where("tbl_users.usr_emailaddress", $email);
        $this->db->where("tbl_users.usr_password", $password);
        $this->db->from("tbl_users");
        return $this->db->get()->row();
    }

    function createSession($data) {
        $_SESSION["lt_main_name"] = $data->usr_name;
        $_SESSION["lt_main_email"] = $data->usr_emailaddress;
    }

    function updatePassword($email, $data) {
        $this->db->where("usr_emailaddress", $email);
        $this->db->update("tbl_users", $data);
        return $this->db->affected_rows();
    }


}
