<?php

class CategoryPicture_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    private function configFiles() {
        //upload an image options
        $config = array();
        $config['upload_path']      = './assets/img/categories';
        $config['create_thumb']     = true;
        $config['allowed_types']    = 'gif|jpg|png';
        $config['max_size']	        = 22000;
        $config['image_width']      = 1024;
        $config['image_height']     = 768;
        $config['maintain_ratio']   = true;
        $config['encrypt_name']     = TRUE;
        return $config;
    }

    function getRecordByCategory($categoryId) {
        $this->db->select("id, category_id, photo, icon");
        $this->db->where("category_id", $categoryId);
        $this->db->from("tbl_category_picture");
        return $this->db->get()->row();
    }

    function deleteImageRecord($name) {
        if (is_file("assets/img/categories/".$name) == 1) {
            unlink("assets/img/categories/".$name);
        }
    }

    function updateRecordPicture($catId, $name, $date) {
        $record = $this->getRecordByCategory($catId);
        $data = array("photo" => $name, "updated_at" => $date);
        if (empty($record)) {
            $data["created_at"] = $date;
            $data["category_id"] = $catId;
            $this->db->insert("tbl_category_picture", $data);
        } else {
            $this->db->where("category_id", $catId);
            $this->db->update("tbl_category_picture", $data);
        }
    }

    function updateRecordIcon($catId, $name, $date) {
        $record = $this->getRecordByCategory($catId);
        $data = array("icon" => $name, "updated_at" => $date);
        if (empty($record)) {
            $data["created_at"] = $date;
            $data["category_id"] = $catId;
            $this->db->insert("tbl_category_picture", $data);
        } else {
            $this->db->where("category_id", $catId);
            $this->db->update("tbl_category_picture", $data);
        }
    }

    function newRecord($data) {
        $this->db->insert("tbl_category_picture", $data);
    }

    function create($file) {
        $this->load->library('upload');
        if (isset($file['photo']) && strlen($file['photo']['name']) > 0) {
            $_FILES = [];
            $_FILES['userfile']['name']     = $file['photo']['name'];
            $_FILES['userfile']['type']     = $file['photo']['type'];
            $_FILES['userfile']['tmp_name'] = $file['photo']['tmp_name'];
            $_FILES['userfile']['error']    = $file['photo']['error'];
            $_FILES['userfile']['size']     = $file['photo']['size'];

            $this->upload->initialize($this->configFiles());
            $this->upload->do_upload();
            $dataPicture = $this->upload->data();

            return $dataPicture["file_name"];
        }
        return "";
    }

    function createIcon($file) {
        if (isset($file['icon']) && strlen($file['icon']['name']) > 0) {
            $_FILES = [];
            $_FILES['userfile']['name']     = $file['icon']['name'];
            $_FILES['userfile']['type']     = $file['icon']['type'];
            $_FILES['userfile']['tmp_name'] = $file['icon']['tmp_name'];
            $_FILES['userfile']['error']    = $file['icon']['error'];
            $_FILES['userfile']['size']     = $file['icon']['size'];

            $this->upload->initialize($this->configFiles());
            $this->upload->do_upload();
            $dataPicture = $this->upload->data();

            return $dataPicture["file_name"];
        }
        return "";
    }
}
