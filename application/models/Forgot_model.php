<?php
class Forgot_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }


    function createRequest($data) {
        $token = password_hash($data->cust_emailaddress, PASSWORD_DEFAULT);
        $today = date("Y-m-d H:i:s");
        $arreglo = array(
            "token"         => $token,
            "created_at"    => $today,
            "updated_at"    => $today,
            "cust_id"  => $data->cust_id,
            "active"     => 1
        );
        $this->db->insert("tbl_forgot", $arreglo);

        $data->token = $token;
        $this->load->model("Email_model");
        $emailBody = $this->Email_model->forgotPasswordBody($data);
//        $this->Email_model->sendEmail($data->cust_emailaddress, $emailBody);
        return $emailBody;
    }

    function updateRecord($id) {
        $data = array("active" => 0);
        $this->db->where("id", $id);
        $this->db->update("tbl_forgot", $data);
    }

    function getRecordByTokenCustomer($token, $customer) {
        $this->db->select("id, cust_id");
        $this->db->like("token", $token);
        $this->db->where("MD5(cust_id)", $customer);
        $this->db->where("active", 1);
        $this->db->where("created_at >", date("Y-m-d H:i:s", strtotime("-1 day")));
        $this->db->from("tbl_forgot");
        return $this->db->get()->row();
    }

    function getRecordByToken($token, $customer) {
        $this->db->select("id, cust_id");
        $this->db->where("token", $token);
        $this->db->where("cust_id", $customer);
        $this->db->where("active", 1);
        $this->db->where("created_at >", date("Y-m-d H:i:s", strtotime("-1 day")));
        $this->db->from("tbl_forgot");
        return $this->db->get()->row();
    }





}
